package de.uni_leipzig.asv.tools.jwarcex.core.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import org.jwat.common.HeaderLine;
import org.jwat.warc.WarcRecord;

import de.uni_leipzig.asv.tools.jwarcex.core.constant.WarcConstants;

/**
 * Provides helper function for working with WarcRecords.
 */
public class WarcRecordUtil {

    private static final String ISO_8601_DATE_STRING = "yyyy-MM-dd'T'HH:mm:ss'Z'";

    private static final String OUTPUT_DATE_FORMAT = "yyyy-MM-dd";

    private DateFormat dateFormatGmt = new SimpleDateFormat(ISO_8601_DATE_STRING);

    private DateFormat dateFormatOutputGmt = new SimpleDateFormat(OUTPUT_DATE_FORMAT, Locale.US);


    public WarcRecordUtil() {

        this.dateFormatGmt.setTimeZone(TimeZone.getTimeZone("GMT"));
        this.dateFormatOutputGmt.setTimeZone(TimeZone.getTimeZone("GMT"));
    }


    /**
     * Parses the date from the dateString of the warcRecord's "Warc-Date" header field and returns
     * a formatted date string (yyyy-MM-dd).
     *
     * <pre>
     * https://webarchive.jira.com/wiki/display/Heritrix/Glossary:
     * "All times in Heritrix are GMT, assuming the clock and timezone on the local system
     * are correct."
     * </pre>
     *
     * @param dateString
     *        a valid iso8601 string (GMT assumed)
     * @return String a formatted string in the output date format yyyy-MM-dd (GMT)
     * @throws ParseException
     *         if the date cannot be parsed
     */
    public String getFormattedDate(String dateString) throws ParseException {

        Date date = this.dateFormatGmt.parse(dateString);
        return this.dateFormatOutputGmt.format(date);
    }



    /**
     * Returns true, if the given warcRecord is a response record. This is the case when the header
     * WARC-Type equals "response".
     *
     * @param warcRecord
     *        a warcRecord
     * @return true is the give record is a response record, false otherwise.
     */
    public boolean isResponseRecord(WarcRecord warcRecord) {

        HeaderLine warcTypeHeaderLine = warcRecord.getHeader(WarcConstants.WARC_HEADER_TYPE_KEY);

        return warcTypeHeaderLine != null && warcTypeHeaderLine.value.equals("response");
    }


    /**
     * Returns the String value of the headerField with the given headerFieldName from the given
     * warcRecord. If there is no headerField with the given name <code>null</code> will be
     * returned.
     *
     * @param warcRecord
     *        a warc record
     * @param headerFieldName
     *        name (key) of the headerField
     * @return header field value as string if the header field is present, otherwise null.
     */
    public String getWarcRecordHeaderFieldValue(WarcRecord warcRecord, String headerFieldName) {

        HeaderLine headerLine = warcRecord.getHeader(headerFieldName);

        if (headerLine != null) {

            return headerLine.value;
        }

        return null;
    }

}
