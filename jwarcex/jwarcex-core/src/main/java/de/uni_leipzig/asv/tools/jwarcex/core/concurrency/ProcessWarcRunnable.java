package de.uni_leipzig.asv.tools.jwarcex.core.concurrency;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.concurrent.BlockingQueue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.uni_leipzig.asv.tools.jwarcex.core.structures.ProcessedWarcDocument;
import de.uni_leipzig.asv.tools.jwarcex.core.structures.RawWarcDocument;
import de.uni_leipzig.asv.tools.jwarcex.core.util.WarcRecordUtil;
import de.uni_leipzig.asv.tools.jwarcex.encoding_detection.EncodingDetector;
import de.uni_leipzig.asv.tools.jwarcex.text_extraction.TextExtractor;

/**
 * Performs all transformation necessary to build a {@link ProcessedWarcDocument} from a
 * {@link RawWarcDocument}.
 */
public class ProcessWarcRunnable implements Runnable {

	private static final Logger LOGGER = LogManager.getLogger(ProcessWarcRunnable.class);

	private final WarcRecordUtil warcRecordUtil = new WarcRecordUtil();

	private final EncodingDetector encodingDetector;

	private final List<RawWarcDocument> rawWarcDocuments;

	private final BlockingQueue<ProcessedWarcDocument> queue;

	private final TextExtractor textExtractor;


	public ProcessWarcRunnable(List<RawWarcDocument> rawWarcDocuments, BlockingQueue<ProcessedWarcDocument> queue,
			EncodingDetector encodingDetector, TextExtractor textExtractor) {

		this.rawWarcDocuments = rawWarcDocuments;
		this.queue = queue;
		this.encodingDetector = encodingDetector;
		this.textExtractor = textExtractor;
	}


	@Override
	public void run() {

		for (RawWarcDocument rawWarcDocument : this.rawWarcDocuments) {

			String encoding = this.getEncoding(rawWarcDocument);

			if (encoding != null) {

				this.process(rawWarcDocument, encoding);
			} else {

				LOGGER.warn("No encoding could be detected for document with location: {}",
						String.valueOf(rawWarcDocument.getLocation()));
			}

		}
	}


	private void process(RawWarcDocument rawWarcDocument, String encoding) {

		ProcessedWarcDocument processedWarcDocument = this.processRawWarcDocument(rawWarcDocument, encoding);

		if (processedWarcDocument != null) {

			this.addProcessedEntryToQueue(processedWarcDocument);
		} else {

			LOGGER.debug("No text extracted from document with location={}", rawWarcDocument.getLocation());
		}
	}


	private String getEncoding(RawWarcDocument rawWarcDocument) {

		return this.encodingDetector.getEncoding(rawWarcDocument.getContent());
	}


	protected ProcessedWarcDocument processRawWarcDocument(RawWarcDocument rawWarcDocument, String encoding) {

		try {

			String content = this.getText(rawWarcDocument, encoding);

			if (content != null) {
				return new ProcessedWarcDocument(rawWarcDocument.getLocation(),
						this.warcRecordUtil.getFormattedDate(rawWarcDocument.getDate()), content, encoding);
			}

			return null;

		} catch (Exception e) {

			LOGGER.warn("Exception during processing of document '{}': {}: {}", rawWarcDocument.getLocation(),
					e.getClass().toString(), e.getMessage());
			return null;
		}

	}


	private String getText(RawWarcDocument rawWarcDocument, String charsetName) throws UnsupportedEncodingException {

		String content = this.createNewStringFromBytesAndCharsetName(rawWarcDocument, charsetName);
		return this.textExtractor.getText(content);
	}


	protected String createNewStringFromBytesAndCharsetName(RawWarcDocument rawWarcDocument, String charsetName)
			throws UnsupportedEncodingException {

		try {

			return new String(rawWarcDocument.getContent(), charsetName);
		} catch (UnsupportedEncodingException e) {

			LOGGER.warn("UnsupportedEncodingException '{}': {}", rawWarcDocument.getLocation(), charsetName);

			throw e;
		}
	}


	protected void addProcessedEntryToQueue(ProcessedWarcDocument sourceFileEntry) {

		boolean success = false;

		while (!success) {

			success = this.queue.offer(sourceFileEntry);
		}
	}
}
