package de.uni_leipzig.asv.tools.jwarcex.core.extract;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import de.uni_leipzig.asv.tools.jwarcex.AssertSourceFile;
import de.uni_leipzig.asv.tools.jwarcex.core.extract.ParallelWarcExtractor;
import de.uni_leipzig.asv.tools.jwarcex.core.extract.WarcExtractor;



public class ParallelWarcExtractorExtractTest {

    private WarcExtractor parallelWarcExtractor = new ParallelWarcExtractor();

    @Rule
    public TemporaryFolder tempFolder = new TemporaryFolder();

    private Path outPath;


    @Before
    public void before() throws IOException {

        this.outPath = new File(this.tempFolder.getRoot(), "out.source").toPath();

        if (Files.exists(this.outPath)) {

            Files.delete(this.outPath);
        }
    }


    @Test
    public void testExtract() throws IOException {

        Path warcPath = Paths.get("src/test/resources/warc/za_web_2015.00206.10records.test.warc");

        Assert.assertFalse(Files.exists(this.outPath));

        this.parallelWarcExtractor.extract(warcPath, this.outPath, new HashMap<String, Object>());

        Assert.assertTrue(Files.exists(this.outPath));
        AssertSourceFile.assertNumEntries(3, this.outPath);
    }


    @Test
    public void testExtractWithStreams() throws IOException {

        Path warcPath = Paths.get("src/test/resources/warc/za_web_2015.00206.10records.test.warc");

        Assert.assertFalse(Files.exists(this.outPath));

        this.parallelWarcExtractor.extract(FileUtils.openInputStream(warcPath.toFile()),
                FileUtils.openOutputStream(this.outPath.toFile()), new HashMap<String, Object>());

        Assert.assertTrue(Files.exists(this.outPath));
        AssertSourceFile.assertNumEntries(3, this.outPath);
    }
}
