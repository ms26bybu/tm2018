package de.uni_leipzig.asv.tools.jwarcex.core.extract;

import java.io.File;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import de.uni_leipzig.asv.tools.jwarcex.core.reader.WarcReaderImpl;
import de.uni_leipzig.asv.tools.jwarcex.core.structures.RawWarcDocument;
import de.uni_leipzig.asv.tools.jwarcex.encoding_detection.EncodingDetector;
import de.uni_leipzig.asv.tools.jwarcex.encoding_detection.EncodingDetectorImpl;
import de.uni_leipzig.asv.tools.jwarcex.text_extraction.TextExtractor;
import de.uni_leipzig.asv.tools.jwarcex.text_extraction.TextExtractorImpl;

@RunWith(MockitoJUnitRunner.class)
public class AbstractWarcExtractorTest {

	@Spy
	private AbstractWarcExtractor abstractWarcExtractor;

	@Rule
	public TemporaryFolder tempFolder = new TemporaryFolder();


	@Test
	public void testExtractUsingPaths() {

		Assert.assertNull(this.abstractWarcExtractor.getOutputStream());

		Path warcPath = Paths.get("src/test/resources/warc/ch_web_2016.00046.3records.test.warc");
		Path outputPath = new File(this.tempFolder.getRoot(), "out.source").toPath();
		Map<String, Object> additionalParameters = new HashMap<>();

		Mockito.when(this.abstractWarcExtractor.createWarcReader(Mockito.any(InputStream.class)))
				.thenReturn(new WarcReaderImpl(warcPath));

		this.abstractWarcExtractor.extract(warcPath, outputPath, additionalParameters);

		Mockito.verify(this.abstractWarcExtractor).processWarc(Mockito.any(RawWarcDocument.class));

		Assert.assertNotNull(this.abstractWarcExtractor.getOutputStream());
	}


	@Test
	public void testGettersAndSetters() {

		Assert.assertNull(this.abstractWarcExtractor.getEncodingDetector());
		Assert.assertNull(this.abstractWarcExtractor.getTextExtractor());

		EncodingDetector encodingDetector = new EncodingDetectorImpl();
		this.abstractWarcExtractor.setEncodingDetector(encodingDetector);

		TextExtractor textExtractor = new TextExtractorImpl(new HashMap<String, Object>());
		this.abstractWarcExtractor.setTextExtractor(textExtractor);
		Assert.assertEquals(encodingDetector, this.abstractWarcExtractor.getEncodingDetector());
		Assert.assertEquals(textExtractor, this.abstractWarcExtractor.getTextExtractor());

	}
}
