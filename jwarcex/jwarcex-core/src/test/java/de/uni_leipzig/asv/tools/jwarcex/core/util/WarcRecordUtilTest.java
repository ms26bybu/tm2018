package de.uni_leipzig.asv.tools.jwarcex.core.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.ParseException;

import org.junit.Assert;
import org.junit.Test;
import org.jwat.common.Diagnosis;
import org.jwat.common.Diagnostics;
import org.jwat.warc.WarcHeader;
import org.jwat.warc.WarcRecord;
import org.jwat.warc.WarcWriterFactory;

import de.uni_leipzig.asv.tools.jwarcex.TestWarcRecords;

public class WarcRecordUtilTest {

    private WarcRecordUtil warcRecordUtil = new WarcRecordUtil();


    @Test
    public void testGetFormattedDate() throws ParseException {

        String dateString = this.warcRecordUtil.getFormattedDate("2016-09-13T23:59:38Z");
        Assert.assertEquals("2016-09-13", dateString);
    }


    @Test(expected = ParseException.class)
    public void testGetFormattedDateWithInvalidDate() throws ParseException {

        this.warcRecordUtil.getFormattedDate("invalid date");
    }


    @Test
    public void testIsResponseRecord() throws IOException {

        try (WarcRecord warcRecord = TestWarcRecords.getSecondWarcRecord()) {

            Assert.assertTrue(this.warcRecordUtil.isResponseRecord(warcRecord));
        }
    }


    @Test
    public void testIsResponseRecordWithoutWarcTypeHeaderField() throws IOException {

        try (WarcRecord warcRecord = TestWarcRecords.getSecondWarcRecord();) {

            // create empty header to reset all fields (remove operation does not exist)
            warcRecord.header = WarcHeader.initHeader(
                    WarcWriterFactory.getWriterUncompressed(new ByteArrayOutputStream()),
                    new Diagnostics<Diagnosis>());

            Assert.assertFalse(this.warcRecordUtil.isResponseRecord(warcRecord));
        }
    }


    @Test
    public void testIsResponseRecordWithDifferentWarcType() throws IOException {

        try (WarcRecord warcRecord = TestWarcRecords.getFirstWarcRecord()) {

            Assert.assertFalse(this.warcRecordUtil.isResponseRecord(warcRecord));
        }
    }
}
