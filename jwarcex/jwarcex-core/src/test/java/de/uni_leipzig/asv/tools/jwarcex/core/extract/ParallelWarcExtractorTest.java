package de.uni_leipzig.asv.tools.jwarcex.core.extract;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.mockito.Mockito;

import de.uni_leipzig.asv.tools.jwarcex.core.structures.RawWarcDocument;

public class ParallelWarcExtractorTest {

	private final ParallelWarcExtractor parallelWarcExtractor = Mockito.spy(ParallelWarcExtractor.class);

	@Rule
	public TemporaryFolder tempFolder = new TemporaryFolder();

	private final Path warcPath = Paths.get("src/test/resources/warc/za_web_2015.00206.10records.test.warc");

	private Path outPath;


	@Before
	public void before() throws IOException {

		this.outPath = new File(this.tempFolder.getRoot(), "out.source").toPath();
	}


	@Test
	public void testExtractArguments() throws IOException {
		Mockito.doNothing().when(this.parallelWarcExtractor).prepare();
		Mockito.doNothing().when(this.parallelWarcExtractor).processWarc(Mockito.any(RawWarcDocument.class));
		Mockito.doNothing().when(this.parallelWarcExtractor).cleanup();

		this.parallelWarcExtractor.extract(this.warcPath, this.outPath, new HashMap<String, Object>());

		Mockito.verify(this.parallelWarcExtractor)
				.setNumberOfThreads(ParallelWarcExtractor.PARAMETER_NUMBER_OF_THREADS_DEFAULT);
	}


	@Test
	public void testExtractArgumentsWithNumThreadsArgument() throws IOException {

		int numThreads = 4;
		Map<String, Object> additionalArguments = new HashMap<String, Object>();
		additionalArguments.put(ParallelWarcExtractor.PARAMETER_NUMBER_OF_THREADS, Integer.valueOf(numThreads));

		Mockito.doNothing().when(this.parallelWarcExtractor).prepare();
		Mockito.doNothing().when(this.parallelWarcExtractor).processWarc(Mockito.any(RawWarcDocument.class));
		Mockito.doNothing().when(this.parallelWarcExtractor).cleanup();

		this.parallelWarcExtractor.extract(this.warcPath, this.outPath, additionalArguments);

		Mockito.verify(this.parallelWarcExtractor).setNumberOfThreads(numThreads);
	}


	@Test
	public void testExtractArgumentsWithInvalidNumThreadsArgument() throws IOException {

		Map<String, Object> additionalArguments = new HashMap<String, Object>();
		additionalArguments.put(ParallelWarcExtractor.PARAMETER_NUMBER_OF_THREADS, "invalid argument here");

		Mockito.doNothing().when(this.parallelWarcExtractor).prepare();
		Mockito.doNothing().when(this.parallelWarcExtractor).processWarc(Mockito.any(RawWarcDocument.class));
		Mockito.doNothing().when(this.parallelWarcExtractor).cleanup();

		this.parallelWarcExtractor.extract(this.warcPath, this.outPath, additionalArguments);

		Mockito.verify(this.parallelWarcExtractor)
				.setNumberOfThreads(ParallelWarcExtractor.PARAMETER_NUMBER_OF_THREADS_DEFAULT);
	}


	@Test
	public void testProcessWarcWithRobotsTxt() throws UnsupportedEncodingException {

		RawWarcDocument rawWarcDocument = new RawWarcDocument("http://www.example.com/robots.txt",
				"2016-09-13T23:59:38Z", "This is my content".getBytes("UTF-8"));

		this.parallelWarcExtractor.processWarc(rawWarcDocument);

		Mockito.verify(this.parallelWarcExtractor, Mockito.times(0)).addToQueue(rawWarcDocument);

	}
}
