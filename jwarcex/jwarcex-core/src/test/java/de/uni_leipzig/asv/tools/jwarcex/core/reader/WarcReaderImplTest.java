package de.uni_leipzig.asv.tools.jwarcex.core.reader;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;


public class WarcReaderImplTest {

    private Path warcPath =
            Paths.get("src/test/resources/warc/cz_web_2013_all.00003.5records.test.warc");
    private Path chPath = Paths.get("src/test/resources/warc/ch_web_2016.00046.3records.test.warc");
    private Path zaPath =
            Paths.get("src/test/resources/warc/za_web_2015.00206.10records.test.warc");

    private Path warcPathToEmptyFile = Paths.get("src/test/resources/warc/empty.warc");


    @Test(expected = IllegalArgumentException.class)
    public void testWithNonExistentFile() throws Exception {

        try (WarcReaderImpl warcReader = new WarcReaderImpl(Paths.get("does/not/exist"))) {

            // nothing here
        }
    }


    @Test
    public void testWithSwissFile() throws Exception {

        try (WarcReaderImpl warcReader = new WarcReaderImpl(this.chPath)) {

            Assert.assertTrue(warcReader.hasNext());
            Assert.assertNotNull(warcReader.next());

            Assert.assertFalse(warcReader.hasNext());
        }
    }



    @Test
    public void testWithSwissFileAndConversionError() throws Exception {


        try (WarcReaderImpl warcReader = new WarcReaderImpl(this.zaPath)) {

            WarcReaderImpl warcReaderSpy = Mockito.spy(warcReader);

            // act per iteration:
            // 1) throw
            // 2) call real method
            // 3) call real method
            Mockito.doThrow(new IOException("for testing purposes")).doCallRealMethod()
                    .doCallRealMethod().when(warcReaderSpy)
                    .toByteArray(Mockito.any(InputStream.class));

            Assert.assertTrue(warcReaderSpy.hasNext());
            Assert.assertNotNull(warcReaderSpy.next());
        }
    }


    @Test
    public void testStreamConstructorWithSwissFile() throws Exception {

        try (WarcReaderImpl warcReader =
                new WarcReaderImpl(new FileInputStream(this.chPath.toFile()))) {

            Assert.assertTrue(warcReader.hasNext());
            Assert.assertNotNull(warcReader.next());

            Assert.assertFalse(warcReader.hasNext());
        }
    }



    @Test
    public void testWithCzechFile() throws Exception {

        try (WarcReaderImpl warcReader = new WarcReaderImpl(this.warcPath)) {

            Assert.assertTrue(warcReader.hasNext());
            Assert.assertNotNull(warcReader.next());

            Assert.assertTrue(warcReader.hasNext());
            Assert.assertNotNull(warcReader.next());

            Assert.assertFalse(warcReader.hasNext());
        }
    }


    @Test
    public void testWithEmptyFile() throws Exception {

        try (WarcReaderImpl warcReader = new WarcReaderImpl(this.warcPathToEmptyFile)) {
            Assert.assertFalse(warcReader.hasNext());
            Assert.assertNull(warcReader.next());
        }
    }


}
