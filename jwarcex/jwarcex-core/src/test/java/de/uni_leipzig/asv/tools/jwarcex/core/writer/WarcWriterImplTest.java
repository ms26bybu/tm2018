package de.uni_leipzig.asv.tools.jwarcex.core.writer;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.junit.Assert;
import org.junit.Test;

import de.uni_leipzig.asv.tools.jwarcex.core.structures.ProcessedWarcDocument;

public class WarcWriterImplTest {

	@Test
	public void testWrite() throws Exception {

		ProcessedWarcDocument processedWarcDocument = new ProcessedWarcDocument("http://localhost/", "2017-01-01",
				"abc", "UTF-8");

		ByteArrayOutputStream outputStream = new ByteArrayOutputStream(16);
		WarcWriter warcWriter = new WarcWriterImpl(outputStream);

		warcWriter.write(processedWarcDocument);
		warcWriter.flush();

		String result = outputStream.toString();
		Assert.assertEquals("<source>"
				+ "<location>http://localhost/</location><date>2017-01-01</date><encoding>UTF-8</encoding></source>\n\n"
				+ "abc\n\n", result);

	}

}
