# Changelog

## [1.2.0] - 25.10.2018

### Added

- The detected encoding will be written to the source file headers again.

### Fixed

- The encoding error detection mechanism now correctly checks against the utf-8 character (instead of utf-16).
- The description of command line parameter -e was corrected.
- Fixed html in javadocs of TextExtractorImpl.

## [1.1.0] - 23.10.2017

### Added

- Added auto detection of encoding errors with a configurable threshold (-e or --max_encoding_errors. If a document contains more than 3 replacement chars, it will be dropped.
- Added parameter for minimum document length (-n or --document_length). See the program help (-h) for more details.
- Ignore entries ending with robots.txt.

### Changed

- Make the minimum line length parameter accessible via the command line (-m or --line_length).
- Improved handling of tables, which results in more readable output of table cell text.

### Fixed

- Fixed the usage of the maven-shade-plugin. The resulting jwarcex-standalone*.jar can now be again used with the `java -jar jwarcex-standalone*.jar` syntax.