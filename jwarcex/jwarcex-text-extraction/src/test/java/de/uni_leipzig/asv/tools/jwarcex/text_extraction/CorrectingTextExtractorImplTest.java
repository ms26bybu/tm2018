package de.uni_leipzig.asv.tools.jwarcex.text_extraction;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

import com.google.common.collect.Maps;

public class CorrectingTextExtractorImplTest {

	private final CorrectingTextExtractor textExtractor = new CorrectingTextExtractor();

	// TODO: Fix the test
//	@Test
//	public void testGetTextWithTooMuchEncodingErrors() throws IOException {
//
//		final String html = new String(Files.readAllBytes(Paths.get("src/test/resources/document1.txt")));
//		Assert.assertNull(textExtractor.getText(html));
//	}

	@Test
	public void testGetTextWithTolerableAmountOfEncodingErrors() throws IOException {

		final String html = new String(Files.readAllBytes(Paths.get("src/test/resources/document2.txt")));
		Assert.assertNotNull(textExtractor.getText(html));
	}

	@Test
	public void testContainsEncodingErrors() {

		final String html = "Text with visible encoding errors ���";

		Assert.assertTrue(textExtractor.containsEncodingErrors(html, 3));
		Assert.assertFalse(textExtractor.containsEncodingErrors(html, 4));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testConstructorWithInvalidParameters() {

		final Map<String, Object> additionalParameters = Maps.newHashMap();

		additionalParameters.put(CorrectingTextExtractor.PARAMETER_MAX_OCCURRENCES, String.valueOf(-2));

		new CorrectingTextExtractor(additionalParameters);
	}

	@Test
	public void testGetText() {

		final Map<String, Object> additionalParameters = Maps.newHashMap();

		additionalParameters.put(TextExtractorImpl.PARAMETER_MIN_DOCUMENT_LENGTH, String.valueOf(0));
		additionalParameters.put(TextExtractorImpl.PARAMETER_MIN_LINE_LENGTH, String.valueOf(0));

		final CorrectingTextExtractor customCorrectingTextExtractor = new CorrectingTextExtractor(additionalParameters);

		final String html = "Text with visible encoding errors ����";
		Assert.assertNull(customCorrectingTextExtractor.getText(html));
	}

	@Test
	public void testGetTextWithCustomThreshold() {

		final Map<String, Object> additionalParameters = Maps.newHashMap();

		additionalParameters.put(TextExtractorImpl.PARAMETER_MIN_DOCUMENT_LENGTH, String.valueOf(0));
		additionalParameters.put(TextExtractorImpl.PARAMETER_MIN_LINE_LENGTH, String.valueOf(0));

		additionalParameters.put(CorrectingTextExtractor.PARAMETER_MAX_OCCURRENCES, String.valueOf(2));

		final CorrectingTextExtractor customCorrectingTextExtractor = new CorrectingTextExtractor(additionalParameters);

		final String html = "Text with visible encoding errors �";
		Assert.assertNotNull(customCorrectingTextExtractor.getText(html));

		final String html2 = "Text with visible encoding errors ��";
		Assert.assertNull(customCorrectingTextExtractor.getText(html2));
	}

	@Test
	public void testGetTextWithDisabledFunctionality() {

		final Map<String, Object> additionalParameters = Maps.newHashMap();

		additionalParameters.put(TextExtractorImpl.PARAMETER_MIN_DOCUMENT_LENGTH, String.valueOf(0));
		additionalParameters.put(TextExtractorImpl.PARAMETER_MIN_LINE_LENGTH, String.valueOf(0));

		additionalParameters.put(CorrectingTextExtractor.PARAMETER_MAX_OCCURRENCES, String.valueOf(-1));

		final CorrectingTextExtractor customCorrectingTextExtractor = new CorrectingTextExtractor(additionalParameters);

		final String html = "Text with visible encoding errors �";
		Assert.assertNotNull(customCorrectingTextExtractor.getText(html));

		final String html2 = "Text with visible encoding errors ���������";
		Assert.assertNotNull(customCorrectingTextExtractor.getText(html2));
	}

}
