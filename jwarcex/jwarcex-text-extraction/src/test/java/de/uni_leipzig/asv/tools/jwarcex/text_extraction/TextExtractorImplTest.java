package de.uni_leipzig.asv.tools.jwarcex.text_extraction;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

import com.google.common.base.Strings;
import com.google.common.collect.Maps;

public class TextExtractorImplTest {

	@Test
	public void testGetTextWithMinLineLengthParameter() throws IOException {

		Map<String, Object> additionalParameters = Maps.newHashMap();
		additionalParameters.put(TextExtractorImpl.PARAMETER_MIN_DOCUMENT_LENGTH, String.valueOf(0));
		additionalParameters.put(TextExtractorImpl.PARAMETER_MIN_LINE_LENGTH, String.valueOf(10));
		TextExtractorImpl textExtractorImpl = new TextExtractorImpl(additionalParameters);

		String html = "<p>" + Strings.repeat("a", 10) + "</p>\n" + "<p>" + Strings.repeat("a", 9) + "</p>";
		String expectedHtml = Strings.repeat("a", 10);

		String returnedText = textExtractorImpl.getText(html);

		Assert.assertNotNull(returnedText);
		Assert.assertEquals(expectedHtml, textExtractorImpl.getText(returnedText));
	}


	@Test
	public void testGetTextWithMinDocumenLengthParameter() throws IOException {

		Map<String, Object> additionalParameters = Maps.newHashMap();
		additionalParameters.put(TextExtractorImpl.PARAMETER_MIN_DOCUMENT_LENGTH, String.valueOf(10));
		additionalParameters.put(TextExtractorImpl.PARAMETER_MIN_LINE_LENGTH, String.valueOf(0));
		TextExtractorImpl textExtractorImpl = new TextExtractorImpl(additionalParameters);

		String html = "<p>" + Strings.repeat("a", 10) + "</p>\n";
		String expectedHtml = Strings.repeat("a", 10);

		String returnedText = textExtractorImpl.getText(html);

		Assert.assertNotNull(returnedText);
		Assert.assertEquals(expectedHtml, textExtractorImpl.getText(returnedText));
	}


	@Test
	public void testGetTextWithMinDocumenLengthParameterAndTextIsTooShort() throws IOException {

		Map<String, Object> additionalParameters = Maps.newHashMap();
		additionalParameters.put(TextExtractorImpl.PARAMETER_MIN_DOCUMENT_LENGTH, String.valueOf(10));
		additionalParameters.put(TextExtractorImpl.PARAMETER_MIN_LINE_LENGTH, String.valueOf(0));
		TextExtractorImpl textExtractorImpl = new TextExtractorImpl(additionalParameters);

		String html = "<p>" + Strings.repeat("a", 9) + "</p>\n";

		String returnedText = textExtractorImpl.getText(html);

		Assert.assertNull(returnedText);
	}


	@Test
	public void testGetTextWithTable() throws IOException {

		Map<String, Object> additionalParameters = Maps.newHashMap();

		additionalParameters.put(TextExtractorImpl.PARAMETER_MIN_DOCUMENT_LENGTH, String.valueOf(0));
		additionalParameters.put(TextExtractorImpl.PARAMETER_MIN_LINE_LENGTH, String.valueOf(0));

		TextExtractorImpl textExtractorImpl = new TextExtractorImpl(additionalParameters);

		String html = new String(Files.readAllBytes(Paths.get("src/test/resources/table1.txt")));
		String text = textExtractorImpl.getText(html);

		// this is not optimal, yet
		String expected = "C1 C2 " + "\n" + "    a b " + "\n" + "  c d";
		Assert.assertEquals(expected, text);
	}


	@Test
	public void testGetTextWithTableSimple() throws IOException {

		Map<String, Object> additionalParameters = Maps.newHashMap();

		additionalParameters.put(TextExtractorImpl.PARAMETER_MIN_DOCUMENT_LENGTH, String.valueOf(0));
		additionalParameters.put(TextExtractorImpl.PARAMETER_MIN_LINE_LENGTH, String.valueOf(0));

		TextExtractorImpl textExtractorImpl = new TextExtractorImpl(additionalParameters);

		String html = new String(Files.readAllBytes(Paths.get("src/test/resources/table2.txt")));
		String text = textExtractorImpl.getText(html);

		String expected = "a b " + "\n" + "  c d " + "\n" + "  e f";
		Assert.assertEquals(expected, text);
	}
}
