package de.uni_leipzig.asv.tools.jwarcex.text_extraction;


/**
 * A TextExtractor parses HTML strings, extracts relevant text and returns it as a contiguous text.
 */
public interface TextExtractor {

    /**
     * Extracts the text from a given html string.
     *
     * @param html
     *        an HTML String
     * @return the extracted text
     */
    String getText(String html);
}
