package de.uni_leipzig.asv.tools.jwarcex.text_extraction;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;

import com.google.common.collect.Sets;

/**
 * Implements a {@link TextExtractor} using JSoup.
 * <p>
 * Additional text cleaning features are as follows:
 * <ul>
 * <li>Misplaced noscript tags (which is invalid html but encountered on some sites) will be
 * filtered.
 * <li>Multiple sequential line breaks (&gt;2) will be compressed.
 * </ul>
 * <p>
 * Some custom behaviour was included to match the previous JWarcEx application:
 * <ul>
 * <li>Preserves whitespace for div and p tags.
 * <li>Only considers text with a length greater or equal than 20.
 * </ul>
 */
public class TextExtractorImpl implements TextExtractor {

	/**
	 * "Minimum characters per Line"-Parameter name.
	 */
	public static final String PARAMETER_MIN_LINE_LENGTH = "minLineLength";

	/**
	 * Only elements with more characters than this length will be added to the document's total text.
	 * Elements with less than this number of characters will be ignored.
	 */
	public static final int PARAMETER_MIN_LINE_LENGTH_DEFAULT = 20;

	/**
	 * "Minimum characters per document"-Parameter.
	 */
	public static final String PARAMETER_MIN_DOCUMENT_LENGTH = "minDocumentLength";

	/**
	 * Default value for minDocumentLength.
	 */
	public static final int PARAMETER_MIN_DOCUMENT_LENGTH_DEFAULT = 80;

	/**
	 * Lower-cased Strings of block-level html elements. These elements create newlines. The <br>
	 * element is handled separately.
	 */
	private static final Set<String> BLOCK_ELEMENTS = Sets.newHashSet("address", "article", "aside", "blockquote",
			"canvas", "dd", "div", "dl", "fieldset", "figcaption", "figure", "footer", "form", "h1", "h2", "h3", "h4",
			"h5", "h6", "header", "hgroup", "hr", "li", "main", "nav", "noscript", "ol", "output", "p", "pre",
			"section", "table", "ul", "video", "tr");

	/**
	 * Lower-cased Strings of table cell html elements.
	 */
	private static final Set<String> TABLE_CELL_ELEMENTS = Sets.newHashSet("td", "th");

	/**
	 * Line break element.
	 */
	private static final String BR_ELEMENT = "br";

	/**
	 * Maximum number of subsequent linebreaks caused by block elements.
	 */
	private static final int MAX_BLOCK_LEVEL_LINE_BREAKS = 2;

	/**
	 * Only elements with more characters than this length will be added to the document's total text.
	 * Elements with less than this number of characters will be ignored
	 */
	private final int minLineLength;

	/**
	 * Documents with less characters will be ignored. Is preceded by minLineLength.
	 */
	private final int minDocumentLength;


	/**
	 * For testing purposes only.
	 */
	protected TextExtractorImpl() {

		this(new HashMap<String, Object>());
	}


	public TextExtractorImpl(Map<String, Object> additionalParameters) {

		this.minLineLength = this.getMinLineLengthParameter(additionalParameters);
		this.validateMinLineLength();

		this.minDocumentLength = this.getMinDocumentLengthParameter(additionalParameters);
		this.validateMinDocumentLength();
	}


	private int getMinDocumentLengthParameter(Map<String, Object> additionalParameters) {

		if (additionalParameters.containsKey(PARAMETER_MIN_DOCUMENT_LENGTH)) {

			return this.getParameter(additionalParameters, PARAMETER_MIN_DOCUMENT_LENGTH);
		}

		return PARAMETER_MIN_DOCUMENT_LENGTH_DEFAULT;
	}


	private int getMinLineLengthParameter(Map<String, Object> additionalParameters) {

		if (additionalParameters.containsKey(PARAMETER_MIN_LINE_LENGTH)) {

			return this.getParameter(additionalParameters, PARAMETER_MIN_LINE_LENGTH);
		}

		return PARAMETER_MIN_LINE_LENGTH_DEFAULT;
	}


	protected int getParameter(Map<String, Object> additionalParameters, String name) {

		String value = additionalParameters.get(name).toString();
		try {
			return Integer.parseInt(value);
		} catch (NumberFormatException e) {

			throw new IllegalArgumentException("Invalid paramater value for " + name + " (" + value + ")");
		}
	}


	private void validateMinLineLength() {

		if (this.minLineLength < -1) {

			throw new IllegalArgumentException(
					"Invalid paramater value for minLineLength + (" + String.valueOf(this.minLineLength) + ")");
		}
	}


	private void validateMinDocumentLength() {
		if (this.minDocumentLength < -1) {

			throw new IllegalArgumentException(
					"Invalid paramater value for minDocumentLength + (" + String.valueOf(this.minDocumentLength) + ")");
		}
	}


	@Override
	public String getText(String text) {

		Document document = Jsoup.parse(text);
		document = this.handleNoscriptTags(document);

		String extractedText = this.getText(document).trim();

		if (extractedText.length() >= this.minDocumentLength) {

			return extractedText;
		}

		return null;
	}


	/**
	 * Remove any <noscript> tags that reside within the <head> of the document. They can per definition
	 * only contain style, link and meta elements (where we do not extract text from).
	 *
	 * This is to avoid a false behaviour for invalid html pages where the creators wrongly use
	 * arbitrary elements inside a head's noscript tag.
	 *
	 * @see https://developer.mozilla.org/en/docs/Web/HTML/Element/noscript
	 */
	private Document handleNoscriptTags(Document document) {

		Elements elements = document.select("head noscript");

		for (Element element : elements) {

			element.remove();
		}

		return document;
	}


	/**
	 * Extracts the text from the given element recursively.
	 *
	 * @param element
	 *            Element to start from
	 * @return the text extracted from the html structure
	 */
	private String getText(Element element) {

		StringBuilder sb = new StringBuilder();

		for (Node node : element.childNodes()) {

			if (node instanceof TextNode) {

				TextNode textNode = (TextNode) node;
				String text = textNode.text();

				if (text.length() >= this.minLineLength) {

					sb.append(text);
				}

			} else if (node instanceof Element) {

				this.processNonTextNode(sb, node);
			}
		}

		return sb.toString();
	}


	private void processNonTextNode(StringBuilder sb, Node node) {

		Element nodeElement = (Element) node;
		String tag = nodeElement.tag().getName().toLowerCase();

		String text = this.getText(nodeElement);

		if (text.length() > 0) {

			if (TABLE_CELL_ELEMENTS.contains(tag) && this.needsSpace(sb)) {

				sb.append(" ");
			}

			sb.append(text);

			boolean lineBreakPossible = sb.length() < MAX_BLOCK_LEVEL_LINE_BREAKS
					|| sb.charAt(sb.length() - MAX_BLOCK_LEVEL_LINE_BREAKS) != '\n';

			if (lineBreakPossible && BLOCK_ELEMENTS.contains(tag)) {

				this.addNewLine(sb);
			}
		} else if (BR_ELEMENT.equals(tag)) {

			this.addNewLine(sb);
		}
	}


	private boolean needsSpace(StringBuilder sb) {

		return sb.length() != 0 && sb.charAt(sb.length() - 1) != '\n' && sb.charAt(sb.length() - 1) != ' ';
	}


	private void addNewLine(StringBuilder sb) {

		sb.append("\n");
	}
}
