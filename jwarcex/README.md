# Readme

## Requirements

* Java 7+
* Maven 3 (only for development)

## Examples

**Print the help**
```
java -jar jwarcex-standalone-<VERSION>-SNAPSHOT.jar -h
```

**Process a WARC file**
```
java -jar jwarcex-standalone-<VERSION>-SNAPSHOT.jar /path/to/my.warc /path/to/output.source
```

**Read gzipped WARCs**
```
java -jar jwarcex-standalone-<VERSION>-SNAPSHOT.jar /path/to/my.warc.gz /path/to/output.source -c
```
