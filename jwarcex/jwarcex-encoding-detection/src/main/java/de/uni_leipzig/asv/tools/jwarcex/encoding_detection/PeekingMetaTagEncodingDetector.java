package de.uni_leipzig.asv.tools.jwarcex.encoding_detection;

import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * Tries to guess the encoding using information from html's meta tags.
 * <p>
 * Only looks at the first N bytes from the content, reads it assuming it is
 * UTF-8 and tries to read the meta tags. This is a fallback strategy, when no
 * encoding could be detected from the byte array.
 */
public class PeekingMetaTagEncodingDetector implements EncodingDetector {

	private static final Logger LOGGER = LogManager.getLogger(PeekingMetaTagEncodingDetector.class);

	private static final Pattern META_WITH_CONTENT_PATTERN = Pattern.compile("(?:^|;)\\s*charset\\s*=([^;]+)");

	private static final String HTML_META_TAG = "meta";

	private static final String HTML_META_ATTRIBUTE_CHARSET = "charset";

	private static final String HTML_META_ATTRIBUTE_CONTENT = "content";

	private final int numBytes;

	public PeekingMetaTagEncodingDetector(final int numBytes) {

		this.numBytes = numBytes;
	}

	@Override
	public String getEncoding(final byte[] data) {

		final int minLength = Math.min(data.length, numBytes);

		final byte[] firstBytes = new byte[numBytes];
		System.arraycopy(data, 0, firstBytes, 0, minLength);

		final String html = new String(firstBytes);
		final Document document = Jsoup.parse(html);

		final Elements elements = document.select(HTML_META_TAG);

		return this.getEncoding(elements);

	}

	private String getEncoding(final Elements elements) {

		final Iterator<Element> iterator = elements.iterator();

		String encoding = null;

		while (iterator.hasNext()) {

			final Element element = iterator.next();

			if (element.hasAttr(HTML_META_ATTRIBUTE_CHARSET)) {

				encoding = element.attr(HTML_META_ATTRIBUTE_CHARSET);
				LOGGER.trace("PeekingMetaTagEncodingDetector: read encoding from charset: {}", encoding);
				return encoding;

			} else if (element.hasAttr(HTML_META_ATTRIBUTE_CONTENT)) {

				final String contentAttribute = element.attr(HTML_META_ATTRIBUTE_CONTENT);
				final Matcher matcher = META_WITH_CONTENT_PATTERN.matcher(contentAttribute);

				if (matcher.find()) {

					encoding = matcher.group(1);
					LOGGER.trace("PeekingMetaTagEncodingDetector: read encoding from content: {}", encoding);
					return encoding;
				}
			}
		}

		return encoding;
	}

}
