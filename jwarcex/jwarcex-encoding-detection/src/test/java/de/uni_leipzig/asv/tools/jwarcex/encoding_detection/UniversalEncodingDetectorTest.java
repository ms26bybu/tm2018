package de.uni_leipzig.asv.tools.jwarcex.encoding_detection;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.junit.Assert;
import org.junit.Test;

import de.uni_leipzig.asv.tools.jwarcex.encoding_detection.EncodingDetector;
import de.uni_leipzig.asv.tools.jwarcex.encoding_detection.UniversalEncodingDetector;



public class UniversalEncodingDetectorTest {

    private EncodingDetector encodingDetector = new UniversalEncodingDetector();


    @Test
    public void testSimpleEncoding() throws IOException {

        byte[] data = Files.readAllBytes(Paths.get("src/test/resources/encoding/cp1252.txt"));
        String encoding = this.encodingDetector.getEncoding(data);
        Assert.assertEquals("WINDOWS-1252", encoding);
    }


    @Test
    public void testChineseEncoding() throws IOException {

        byte[] data = Files.readAllBytes(Paths.get("src/test/resources/encoding/gb2312.txt"));
        String encoding = this.encodingDetector.getEncoding(data);
        Assert.assertEquals("GB18030", encoding);
    }


    @Test
    public void testNotRecognizedUtf8Encoding() throws IOException {

        byte[] data = Files.readAllBytes(Paths.get("src/test/resources/encoding/ch1.txt"));
        String encoding = this.encodingDetector.getEncoding(data);
        Assert.assertNull(encoding);
    }
}
