package de.uni_leipzig.asv.tools.jwarcex.encoding_detection;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.junit.Assert;
import org.junit.Test;



public class PeekingMetaTagEncodingDetectorTest {

    private EncodingDetector encodingDetector = new PeekingMetaTagEncodingDetector(500);


    @Test
    public void testMetaTagWithoutHtml() throws IOException {

        byte[] data = Files.readAllBytes(Paths.get("src/test/resources/encoding/cp1252.txt"));
        String encoding = this.encodingDetector.getEncoding(data);
        Assert.assertEquals(null, encoding);
    }


    @Test
    public void testMetaTagWithCharset() throws IOException {

        byte[] data = Files.readAllBytes(Paths.get("src/test/resources/encoding/cp1252-2.txt"));
        String encoding = this.encodingDetector.getEncoding(data);
        Assert.assertEquals("windows-1252", encoding);
    }


    @Test
    public void testMetaTagWithContent() throws IOException {

        byte[] data = Files.readAllBytes(Paths.get("src/test/resources/encoding/cp1252-3.txt"));
        String encoding = this.encodingDetector.getEncoding(data);
        Assert.assertEquals("windows-1252", encoding);
    }
}
