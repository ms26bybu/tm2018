package de.uni_leipzig.asv.tools.jwarcex.encoding_detection;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

public class EncodingDetectorImplTest {

    private EncodingDetector encodingDetectorMock = Mockito.mock(EncodingDetectorImpl.class);

    private EncodingDetector encodingDetectorImpl =
            new EncodingDetectorImpl(Arrays.asList(this.encodingDetectorMock));


    @Test
    public void testWhenEncodingDetectorElementReturnsEncoding() throws IOException {

        byte[] data = Files.readAllBytes(Paths.get("src/test/resources/encoding/gb2312.txt"));
        String chineseCharsetGb = "GB18030";
        Mockito.when(this.encodingDetectorMock.getEncoding(data)).thenReturn(chineseCharsetGb);

        String encoding = this.encodingDetectorImpl.getEncoding(data);
        Assert.assertEquals(chineseCharsetGb, encoding);
    }


    @Test
    public void testWhenEncodingDetectorElementReturnNull() throws IOException {


        byte[] data = Files.readAllBytes(Paths.get("src/test/resources/encoding/cp1252.txt"));
        Mockito.when(this.encodingDetectorMock.getEncoding(data)).thenReturn(null);

        String encoding = this.encodingDetectorImpl.getEncoding(data);
        Assert.assertEquals(EncodingDetectorImpl.CHARSET_NAME_FALLBACK, encoding);
    }


}
