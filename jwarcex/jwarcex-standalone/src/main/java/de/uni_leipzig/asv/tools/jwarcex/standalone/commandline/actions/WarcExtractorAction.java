package de.uni_leipzig.asv.tools.jwarcex.standalone.commandline.actions;

import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.cli.CommandLine;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.uni_leipzig.asv.tools.jwarcex.core.extract.ParallelWarcExtractor;
import de.uni_leipzig.asv.tools.jwarcex.text_extraction.CorrectingTextExtractor;
import de.uni_leipzig.asv.tools.jwarcex.text_extraction.TextExtractorImpl;

public class WarcExtractorAction implements CommandLineAction {

	private static final Logger LOGGER = LogManager.getLogger(WarcExtractorAction.class);

	/**
	 * Default value for the number of threads parameter (= number of warc extraction threads).
	 */
	public static final int DEFAULT_NUMBER_OF_THREADS = ParallelWarcExtractor.PARAMETER_NUMBER_OF_THREADS_DEFAULT;

	/**
	 * Default value for the minimum line length paramater.
	 */
	public static final int DEFAULT_MIN_LINE_LENGTH = TextExtractorImpl.PARAMETER_MIN_LINE_LENGTH_DEFAULT;

	/**
	 * Default value for the minimum document length paramater.
	 */
	public static final int DEFAULT_MIN_DOCUMENT_LENGTH = TextExtractorImpl.PARAMETER_MIN_DOCUMENT_LENGTH_DEFAULT;

	/**
	 * Smallest valid number to accept as a number of threads value.
	 */
	public static final int MINIMUM_NUMBER_OF_THREADS = 1;

	/**
	 * Default value for the compression parameter.
	 */
	public static final boolean DEFAULT_IS_COMPRESSED = false;

	/**
	 * Value for the number of threads parameter. The specified number refers to the additional threads
	 * used for the warc extraction, not the total threads of the application.
	 */
	private int numberOfThreads = DEFAULT_NUMBER_OF_THREADS;


	@Override
	public void execute(CommandLine commandLine, String[] remainingArgs) {

		try {

			this.processArgsForWarcExtraction(commandLine, remainingArgs);

		} catch (Exception e) {

			LOGGER.error("Uncaught exception during warcExtraction: {}", e);
		}
	}


	private void processArgsForWarcExtraction(CommandLine commandLine, String[] remainingArgs) {

		WarcExtractorAdditionalParameters additionalParameters = this.getAdditionalParameters(commandLine);

		if (commandLine.hasOption("t")) {

			this.numberOfThreads = this.parseNumberOfThreads(commandLine.getOptionValue("t"));
		}

		if (commandLine.hasOption("s")) {

			this.extractWarcUsingStreams(System.in, System.out, this.numberOfThreads, additionalParameters);

		} else {

			Path warcPath = Paths.get(remainingArgs[0]);
			Path extractedFilePath = Paths.get(remainingArgs[1]);
			this.extractWarc(warcPath, extractedFilePath, this.numberOfThreads, additionalParameters);
		}
	}


	private WarcExtractorAdditionalParameters getAdditionalParameters(CommandLine commandLine) {

		boolean isCompressed = commandLine.hasOption("c");

		Integer minLineLength = null;
		if (commandLine.hasOption("m")) {

			minLineLength = this.parseIntParameter(commandLine.getOptionValue("m"), "minLineLength");
		}

		Integer minDocumentLength = null;
		if (commandLine.hasOption("n")) {

			minDocumentLength = this.parseIntParameter(commandLine.getOptionValue("n"), "minDocumentLength");
		}

		Integer maxEncodingErrors = null;
		if (commandLine.hasOption("e")) {

			maxEncodingErrors = this.parseIntParameter(commandLine.getOptionValue("e"),
					CorrectingTextExtractor.PARAMETER_MAX_OCCURRENCES);
		}

		return new WarcExtractorAdditionalParameters(isCompressed, minLineLength, minDocumentLength, maxEncodingErrors);
	}


	private int parseNumberOfThreads(String numberOfThreadsString) {

		int numThreads = ParallelWarcExtractor.PARAMETER_NUMBER_OF_THREADS_DEFAULT;

		try {

			numThreads = Integer.parseInt(numberOfThreadsString);
		} catch (Exception e) {

			LOGGER.info("Could not parse threads parameter. Using default value.");
		}

		if (numThreads < MINIMUM_NUMBER_OF_THREADS) {

			LOGGER.info("Invalid value for parameter 'number of threads': {}. Using {} as new value.",
					Integer.valueOf(numThreads), Integer.valueOf(MINIMUM_NUMBER_OF_THREADS));
			numThreads = MINIMUM_NUMBER_OF_THREADS;
		}

		return numThreads;
	}


	private Integer parseIntParameter(String value, String parameterDescription) {

		try {

			return Integer.parseInt(value);
		} catch (Exception e) {

			LOGGER.error("Could not parse '" + parameterDescription + "' parameter. Using default value.");
			return null;
		}
	}


	protected void extractWarc(Path warcPath, Path extractedFilePath, int numThreads,
			WarcExtractorAdditionalParameters additionalParameters) {

		Map<String, Object> additionalParametersMap = this.createAdditionalParametersMap(numThreads,
				additionalParameters);
		ParallelWarcExtractor warcExtractor = this.createNewWarcExtractor(additionalParametersMap);

		warcExtractor.extract(warcPath, extractedFilePath, additionalParametersMap);
	}


	protected void extractWarcUsingStreams(InputStream inputStream, OutputStream outputStream, int numThreads,
			WarcExtractorAdditionalParameters additionalParameters) {

		Map<String, Object> additionalParametersMap = this.createAdditionalParametersMap(numThreads,
				additionalParameters);
		ParallelWarcExtractor warcExtractor = this.createNewWarcExtractor(additionalParametersMap);

		warcExtractor.extract(inputStream, outputStream, additionalParametersMap);
	}


	private Map<String, Object> createAdditionalParametersMap(int numThreads,
			WarcExtractorAdditionalParameters additionalParameters) {

		Map<String, Object> additionalParametersMap = new HashMap<>();
		additionalParametersMap.put(ParallelWarcExtractor.PARAMETER_NUMBER_OF_THREADS, Integer.valueOf(numThreads));
		additionalParametersMap.put(ParallelWarcExtractor.PARAMETER_IS_COMPRESSED,
				Boolean.valueOf(additionalParameters.isCompressed()));

		if (additionalParameters.getMinLineLength() != null) {

			additionalParametersMap.put(TextExtractorImpl.PARAMETER_MIN_LINE_LENGTH,
					Integer.valueOf(additionalParameters.getMinLineLength()));

		}

		if (additionalParameters.getMinDocumentLength() != null) {

			additionalParametersMap.put(TextExtractorImpl.PARAMETER_MIN_DOCUMENT_LENGTH,
					Integer.valueOf(additionalParameters.getMinDocumentLength()));
		}

		if (additionalParameters.getMaxEncodingErrors() != null) {

			additionalParametersMap.put(CorrectingTextExtractor.PARAMETER_MAX_OCCURRENCES,
					Integer.valueOf(additionalParameters.getMaxEncodingErrors()));
		}

		return additionalParametersMap;
	}


	private ParallelWarcExtractor createNewWarcExtractor(Map<String, Object> additionalParameters) {

		return new ParallelWarcExtractor(additionalParameters);
	}
}
