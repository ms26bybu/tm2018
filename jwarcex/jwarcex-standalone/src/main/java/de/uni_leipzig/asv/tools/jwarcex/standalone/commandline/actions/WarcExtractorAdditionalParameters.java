package de.uni_leipzig.asv.tools.jwarcex.standalone.commandline.actions;

public class WarcExtractorAdditionalParameters {

	private final boolean isCompressed;
	private final Integer minLineLength;
	private final Integer minDocumentLength;
	private final Integer maxEncodingErrors;


	public WarcExtractorAdditionalParameters(boolean isCompressed, Integer minLineLength, Integer minDocumentLength,
			Integer maxEncodingErrors) {

		this.isCompressed = isCompressed;
		this.minLineLength = minLineLength;
		this.minDocumentLength = minDocumentLength;
		this.maxEncodingErrors = maxEncodingErrors;
	}


	public boolean isCompressed() {

		return this.isCompressed;
	}


	public Integer getMinLineLength() {

		return this.minLineLength;
	}


	public Integer getMinDocumentLength() {

		return this.minDocumentLength;
	}


	public Integer getMaxEncodingErrors() {

		return this.maxEncodingErrors;
	}

}
