package de.tm2018.encodings;

/**
 * A supplier of {@link WebRecordEncodingAnalyser} objects.
 *
 * @author Michael Schmidt
 *
 */
@FunctionalInterface
public interface WebRecordEncodingAnalyserSupplier {

	/**
	 * Supplies a {@link WebRecordEncodingAnalyser}.
	 * <p>
	 * The returned analyser MUST be closed by whomever requested it.
	 *
	 * @return An analyser, guaranteed to be not {@code null}.
	 */
	WebRecordEncodingAnalyser supplyAnalyser();

}
