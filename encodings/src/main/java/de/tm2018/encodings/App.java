package de.tm2018.encodings;

import static de.tm2018.encodings.detector.WebRecordEncodingDetector.adapt;
import static de.tm2018.encodings.detector.WebRecordEncodingDetector.sanitise;
import static de.tm2018.encodings.util.IOUtil.WEB_ARCHIVES_FILTER;
import static de.tm2018.encodings.util.IOUtil.getFiles;
import static de.tm2018.encodings.util.Util.runParallel;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import de.tm2018.encodings.detector.ApacheTikaParserEncodingDetector;
import de.tm2018.encodings.detector.HttpHeaderEncodingDetector;
import de.tm2018.encodings.detector.ICU4JEncodingDetector;
import de.uni_leipzig.asv.tools.jwarcex.encoding_detection.PeekingMetaTagEncodingDetector;
import de.uni_leipzig.asv.tools.jwarcex.encoding_detection.UniversalEncodingDetector;

/**
 * Hello world!
 *
 */
public class App {

	public static void main(final String[] args) {
		// the start directory in which all web-archives will be extracted and analysed
		final String path = "D:\\tm\\warcs";

		// get files
		// this also includes files in sub-directories
		final List<File> files = getFiles(new File(path), WEB_ARCHIVES_FILTER);

		// print files
		System.out.println("Found " + files.size() + " archive(s)");
		for (final File archive : files) {
			System.out.println(archive.getAbsolutePath());
		}

		// create tasks
		final List<Runnable> tasks = new ArrayList<>();
		for (final File archive : files) {
			tasks.add(() -> {
				// analysers cannot be shared between threads
				analyseArchive(archive.getAbsolutePath(), createAnalyser());

				// just because I have no trust whatsoever in the Java GC.
				System.gc();
			});
		}

		// run tasks in parallel
		runParallel(8, tasks);

		System.out.println("Complete.");
	}

	/**
	 * Creates a {@link EncodingAnalyser}.
	 *
	 * @return
	 */
	private static EncodingAnalyser createAnalyser() {
		@SuppressWarnings("resource")
		final EncodingAnalyser analyser = new EncodingAnalyser();
		analyser.addDetector("universal", adapt(new UniversalEncodingDetector()));
		analyser.addDetector("markup1024", sanitise(adapt(new PeekingMetaTagEncodingDetector(1024))));
		analyser.addDetector("http", sanitise(new HttpHeaderEncodingDetector()));
		analyser.addDetector("apache", new ApacheTikaParserEncodingDetector());
		analyser.addDetector("icu4j", new ICU4JEncodingDetector());

		// disable auto print
		analyser.autoTimingPrintInterval = 0;

		return analyser;
	}

	/**
	 * Analyses the given archive with the given analyser.
	 * <p>
	 * A new {@code .csv} will be created with the same name as the archive in the
	 * same parent directory. An existing {@code .csv} file with the same name will
	 * be overwritten.
	 * <p>
	 * Please note that temporary files will be created in the process.
	 *
	 * @param archive
	 * @param analyser
	 */
	private static void analyseArchive(final String archive, final EncodingAnalyser analyser) {
		System.out.println("Analysing archive " + archive);
		try (ArchiveAnalysisWriter writer = new ArchiveAnalysisWriter(archive, analyser)) {
			writer.write();
			System.out.println("Completed " + archive);
		} catch (final Exception e) {
			System.err.println("Failed to analyse archive " + archive);
			e.printStackTrace();
		}
	}

}
