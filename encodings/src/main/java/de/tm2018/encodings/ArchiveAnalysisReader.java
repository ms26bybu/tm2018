package de.tm2018.encodings;

import static de.tm2018.encodings.util.IOUtil.throwRuntime;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Objects.requireNonNull;

import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.function.Consumer;

import de.siegmar.fastcsv.reader.CsvParser;
import de.siegmar.fastcsv.reader.CsvReader;
import de.siegmar.fastcsv.reader.CsvRow;
import de.tm2018.encodings.util.UpperCaseStringReuser;

public class ArchiveAnalysisReader implements AutoCloseable {

	private final File file;
	private final CsvReader reader = createDefaultCsvReader();
	private final UpperCaseStringReuser reuser = new UpperCaseStringReuser();

	private CsvParser parser;

	public ArchiveAnalysisReader(final File file) {
		this.file = requireNonNull(file);
	}

	private static CsvReader createDefaultCsvReader() {
		final CsvReader reader = new CsvReader();

		reader.setErrorOnDifferentFieldCount(true);
		reader.setContainsHeader(false); // this is really important to be false

		return reader;
	}

	public File getFile() {
		return file;
	}

	public CsvReader getCsvReader() {
		return reader;
	}

	public Iterable<AnalysisResult> read(final Consumer<List<String>> detectorNamesConsumer) throws IOException {
		parser = reader.parse(file, UTF_8);

		// read header
		final CsvRow header = parser.nextRow(); // header
		if (detectorNamesConsumer != null) {
			final ArrayList<String> detectorNames = new ArrayList<>();
			for (int i = 2; i < header.getFieldCount(); i++)
				detectorNames.add(header.getField(i));
			detectorNamesConsumer.accept(detectorNames);
		}

		return ArchiveAnalysisReaderIterator::new;
	}

	@Override
	public void close() throws IOException {
		if (parser != null)
			parser.close();
	}

	private class ArchiveAnalysisReaderIterator implements Iterator<AnalysisResult> {

		private CsvRow row;
		private AnalysisResult res;

		@Override
		public boolean hasNext() {
			try {
				row = parser.nextRow();
				res = toAnalysisResult();
			} catch (final IOException e) {
				throw throwRuntime(e);
			}
			return row != null;
		}

		@Override
		public AnalysisResult next() {
			if (row == null)
				throw new NoSuchElementException();

			return res;
		}

		private AnalysisResult toAnalysisResult() {
			if (row == null)
				return null;

			final String url = row.getField(0);
			final Instant date = toInstant(row.getField(1));

			final int fieldCount = row.getFieldCount();
			final ArrayList<String> encodings = new ArrayList<>(fieldCount - 2);
			for (int i = 2; i < fieldCount; i++) {
				final String value = row.getField(i);
				if (value.length() == 0) {
					encodings.add(null);
				} else {
					encodings.add(reuser.toUpperCase(row.getField(i)));
				}
			}

			return new AnalysisResult(url, date, encodings);
		}

		private Instant toInstant(final String str) {
			return Instant.ofEpochMilli(Long.parseLong(str));
		}

	}

}
