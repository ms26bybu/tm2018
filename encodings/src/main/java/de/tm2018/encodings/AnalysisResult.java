package de.tm2018.encodings;

import static java.util.Objects.requireNonNull;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

public class AnalysisResult {

	String url;

	Instant date;

	List<String> encodings;

	public AnalysisResult(final String url, final Instant date, final List<String> encodings) {
		this.url = requireNonNull(url);
		this.date = requireNonNull(date);
		this.encodings = requireNonNull(encodings);
	}

	public AnalysisResult(final AnalysisResult toCopy) {
		url = toCopy.url;
		date = toCopy.date;
		encodings = new ArrayList<>(toCopy.encodings);
	}

	public String getUrl() {
		return url;
	}

	public Instant getDate() {
		return date;
	}

	public List<String> getEncodings() {
		return encodings;
	}

}
