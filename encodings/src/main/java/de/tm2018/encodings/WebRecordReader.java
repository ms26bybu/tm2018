package de.tm2018.encodings;

import static de.tm2018.encodings.util.IOUtil.throwRuntime;
import static de.tm2018.encodings.util.IOUtil.toByteArray;
import static java.util.Objects.requireNonNull;
import static org.jwat.warc.WarcConstants.FN_WARC_DATE;
import static org.jwat.warc.WarcConstants.FN_WARC_TARGET_URI;
import static org.jwat.warc.WarcConstants.FN_WARC_TYPE;
import static org.jwat.warc.WarcConstants.RT_RESPONSE;

import java.io.IOException;
import java.time.Instant;
import java.util.Iterator;
import java.util.NoSuchElementException;

import org.jwat.common.HttpHeader;
import org.jwat.warc.WarcReader;
import org.jwat.warc.WarcRecord;

public class WebRecordReader implements AutoCloseable {

	private final WarcReader reader;

	public WebRecordReader(final WarcReader reader) {
		this.reader = requireNonNull(reader);
	}

	/**
	 * Returns the next web record or {@code null} if no further records are
	 * available.
	 *
	 * @return An {@link WebRecord} or {@code null}.
	 * @throws IOException
	 */
	@SuppressWarnings("resource")
	public WebRecord getNextRecord() throws IOException {
		WarcRecord rec;
		while ((rec = reader.getNextRecord()) != null) {
			final String type = rec.getHeader(FN_WARC_TYPE).value;
			if (RT_RESPONSE.equals(type)) {
				return toWebRecord(rec);
			}
		}

		return null;
	}

	@SuppressWarnings("resource")
	private static WebRecord toWebRecord(final WarcRecord warcRec) throws IOException {
		// URL from "WARC-Target-URI"
		final String url = warcRec.getHeader(FN_WARC_TARGET_URI).value;

		// parse Instant from "WARC-Date"
		final Instant date = Instant.parse(warcRec.getHeader(FN_WARC_DATE).value);

		final HttpHeader header = warcRec.getHttpHeader();
		final byte[] content = toByteArray(warcRec.getPayloadContent());

		return new WebRecord(url, date, header, content);
	}

	@Override
	public void close() {
		reader.close();
	}

	/**
	 * Returns an {@link Iterable} of the reader for easier use.
	 * <p>
	 * <b>DO NOT</b> use {@link #getNextRecord()} when iterating as it will make the
	 * returned records of both unpredictable.
	 *
	 * @return An {@link Iterable} using {@link #getNextRecord()} to move forward.
	 */
	public Iterable<WebRecord> sequential() {
		return WebRecordReaderIterator::new;
	}

	private class WebRecordReaderIterator implements Iterator<WebRecord> {

		private WebRecord rec;

		@Override
		public boolean hasNext() {
			try {
				rec = getNextRecord();
			} catch (final IOException e) {
				throw throwRuntime(e);
			}
			return rec != null;
		}

		@Override
		public WebRecord next() {
			if (rec == null)
				throw new NoSuchElementException();
			return rec;
		}

	}

}
