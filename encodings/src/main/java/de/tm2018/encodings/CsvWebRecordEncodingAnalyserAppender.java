package de.tm2018.encodings;

import java.io.File;

@FunctionalInterface
public interface CsvWebRecordEncodingAnalyserAppender {

	/**
	 * Returns a {@link WebRecordEncodingAnalyserSupplier} appending a given file
	 * with an existing supplier.
	 *
	 * @param file     The CSV file containing existing data. This file might not
	 *                 exist.
	 * @param supplier The existing supplier.
	 * @return
	 */
	WebRecordEncodingAnalyserSupplier append(File file, WebRecordEncodingAnalyserSupplier supplier);

}
