package de.tm2018.encodings.util;

import static de.tm2018.encodings.util.IOUtil.throwRuntime;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.jwat.warc.WarcReader;
import org.jwat.warc.WarcReaderFactory;

import de.tm2018.encodings.WebRecordReader;

public final class Util {

	private Util() {
	}

	private static final int DEFAULT_BUFFER_SIZE = 1024 * 1204; // 1 MB

	/**
	 * Creates a new {@link WebRecordReader} discerning by itself where the given
	 * file is a compressed {@code .warc} file.
	 *
	 * @param file The {@code .warc} file.
	 * @return A new reader.
	 * @throws IOException
	 */
	public static WebRecordReader webRecordReaderFromFile(final File file) throws IOException {
		return webRecordReaderFromFile(file, DEFAULT_BUFFER_SIZE);
	}

	/**
	 * Creates a new {@link WebRecordReader} discerning by itself where the given
	 * file is a compressed {@code .warc} file.
	 *
	 * @param file       The {@code .warc} file.
	 * @param bufferSize The size of the input stream buffer.
	 * @return A new reader.
	 * @throws IOException
	 */
	@SuppressWarnings("resource")
	public static WebRecordReader webRecordReaderFromFile(final File file, final int bufferSize) throws IOException {
		final String name = file.getName();
		if (!name.contains(".warc"))
			throw new IllegalArgumentException("The given file has to be a warc file.");

		final BufferedInputStream inStream = new BufferedInputStream(new FileInputStream(file), bufferSize);

		WarcReader warcReader;
		if (name.endsWith(".gz"))
			warcReader = WarcReaderFactory.getReaderCompressed(inStream);
		else
			warcReader = WarcReaderFactory.getReaderUncompressed(inStream);

		return new WebRecordReader(warcReader);
	}

	/**
	 * Runs the given runnables in parallel and waits for all of them to complete.
	 *
	 * @param degreeOfParallelism The maximum number of runnables in parallel at
	 *                            each given time.
	 * @param runnables           The list of runnables to execute in parallel.
	 */
	public static void runParallel(final int degreeOfParallelism, final Iterable<Runnable> runnables) {
		final ExecutorService executor = Executors.newFixedThreadPool(degreeOfParallelism);

		for (final Runnable runnable : runnables) {
			executor.execute(runnable);
		}

		// wait until all task complete
		executor.shutdown();
		try {
			executor.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);
		} catch (final InterruptedException e) {
			throw throwRuntime(e);
		}
	}

}
