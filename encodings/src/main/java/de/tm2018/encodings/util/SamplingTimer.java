package de.tm2018.encodings.util;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class SamplingTimer {

	private static final int DEFAULT_CAPACITY = 1024;

	private int size = 0;
	private int index = 0;
	private final long[] samples;

	public SamplingTimer() {
		this(DEFAULT_CAPACITY);
	}

	public SamplingTimer(final int capacity) {
		samples = new long[capacity];
	}

	public void measur(final Runnable task) {
		long millis = System.currentTimeMillis();
		task.run();
		millis = System.currentTimeMillis() - millis;

		addSample(millis * 1_000_000);
	}

	private void addSample(final long duration) {
		samples[index++] = duration;

		if (index == samples.length) {
			index = 0;
		}
		if (size < samples.length) {
			size++;
		}
	}

	private List<Long> getSamples() {
		final ArrayList<Long> list = new ArrayList<>(samples.length);

		final int max = size;
		final int startIndex = index;
		for (int i = 0; i < max; i++) {
			list.add(samples[(i + startIndex) % samples.length]);
		}

		return list;
	}

	public double getEllapsedMilliSeconds() {
		final int count = size;
		if (count == 0)
			return Double.NaN;

		BigInteger sum = BigInteger.ZERO;
		for (final Long sample : getSamples()) {
			sum = sum.add(BigInteger.valueOf(sample));
		}

		final BigInteger[] divAndRem = sum.divideAndRemainder(BigInteger.valueOf(count));
		final double nanos = divAndRem[0].doubleValue() + divAndRem[1].doubleValue() / count;

		return nanos / 1_000_000;
	}

}
