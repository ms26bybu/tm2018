package de.tm2018.encodings.util;

import java.util.Map;
import java.util.TreeMap;

public class UpperCaseStringReuser {

	private final Map<String, String> cache = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);

	public String toUpperCase(final String str) {
		if (str == null)
			return null;
		return cache.computeIfAbsent(str, String::toUpperCase);
	}

}
