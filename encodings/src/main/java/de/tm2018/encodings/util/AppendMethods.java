package de.tm2018.encodings.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import de.tm2018.encodings.AnalysisResult;
import de.tm2018.encodings.ArchiveAnalysisReader;
import de.tm2018.encodings.CsvWebRecordEncodingAnalyserAppender;
import de.tm2018.encodings.WebRecord;
import de.tm2018.encodings.WebRecordEncodingAnalyser;
import de.tm2018.encodings.WebRecordEncodingAnalyserSupplier;

public class AppendMethods {

	private AppendMethods() {
		// private to force them to use the static factory methods
	}

	/**
	 * This will append the data of the {@link WebRecordEncodingAnalyser} by
	 * creating new columns for new detectors and overwriting the content of
	 * existing columns for detectors which are already present in the file.
	 * <p>
	 * Example:<br>
	 *
	 * <pre>
	 * <code>
	 * File:      A | B | D
	 * Analyser:  B | C
	 * Result:    A | B | D | C  where the analyser's B is used
	 * </code>
	 * </pre>
	 *
	 * @return An appender implementing the described behaviour.
	 */
	public static CsvWebRecordEncodingAnalyserAppender overwriteColumns() {
		return (file, supplier) -> () -> new AppendCsvWebRecordEncodingAnalyser(file, supplier);
	}

	private static class AppendCsvWebRecordEncodingAnalyser implements WebRecordEncodingAnalyser {

		private final ArchiveAnalysisReader reader;
		private final WebRecordEncodingAnalyser analyser;
		private final List<String> names = new ArrayList<>();
		private final Iterator<AnalysisResult> iter;

		// a field to optimize the analyse method
		private final ArrayList<String> analyserResults = new ArrayList<>();
		private final Map<String, Integer> selection = new LinkedHashMap<>();
		private static final String ANALYZE = "ANALYZE";
		private static final String NAMES = "NAMES";

		public AppendCsvWebRecordEncodingAnalyser(final File csv, final WebRecordEncodingAnalyserSupplier supplier) {
			reader = new ArchiveAnalysisReader(csv);
			try {
				iter = reader.read(names::addAll).iterator();
			} catch (final IOException e) {
				throw IOUtil.throwRuntime(e);
			}

			analyser = supplier.supplyAnalyser();
			createMergeMap();

		}

		@Override
		public void close() throws Exception {
			reader.close();
			analyser.close();
		}

		@Override
		public List<String> getNames() {
			return names;
		}

		@Override
		public void analyse(final WebRecord record, final Collection<String> reults) {
			analyserResults.clear();
			analyser.analyse(record, analyserResults);

			if (!iter.hasNext())
				throw new IllegalStateException("The CSV file contains less entries than the archive.");
			final AnalysisResult csvResult = iter.next();

			if (!record.url.equals(csvResult.getUrl()))
				throw new IllegalStateException("The CSV file has a different entry order than the archive.");

			for (final Map.Entry<String, Integer> resultGenerator : selection.entrySet()) {
				if (resultGenerator.getKey().equals(ANALYZE)) {
					reults.add(analyserResults.get(resultGenerator.getValue()));
				} else {
					reults.add(csvResult.getEncodings().get(resultGenerator.getValue()));
				}
			}

		}

		public void createMergeMap() {
			final Set<String> temp = new HashSet<>();
			final List<String> tempAnalyzes = analyser.getNames();
			for (final String name : names) {
				if (tempAnalyzes.contains(name)) {
					selection.put(ANALYZE, tempAnalyzes.indexOf(name));
					temp.add(name);
				} else {
					selection.put(NAMES, names.indexOf(name));
				}
			}
			for (final String name : tempAnalyzes) {
				if (!temp.contains(name)) {
					selection.put(NAMES, tempAnalyzes.indexOf(name));
				}
			}
		}
	}
}
