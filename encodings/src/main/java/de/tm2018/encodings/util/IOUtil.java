package de.tm2018.encodings.util;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import com.google.common.io.ByteStreams;

public final class IOUtil {

	/**
	 * A filename filter which will go recursively through all sub directories
	 * return true for all {@code .warc} and {@code .warc.gz} files.
	 */
	public static final FilenameFilter WEB_ARCHIVES_FILTER = recursiveFilenameFiler(".*\\.warc(?:\\.gz)?");

	/**
	 * A recursive filename filter matching {@code .csv} files.
	 */
	public static final FilenameFilter CSV_FILTER = recursiveFilenameFiler(".*\\.csv");

	private IOUtil() {
	}

	/**
	 * Returns recursively all files (not directories) which are under the given
	 * parent directory.
	 *
	 * @param directory The parent directory from which the recursive search for
	 *                  files will start.
	 * @return A list of all files under the given directory.
	 * @throws IllegalArgumentException if {@code directory} is not a directory.
	 * @throws NullPointerException     if {@code directory} is {@code null}.
	 */
	public static List<File> getFiles(final File directory) {
		return getFiles(directory, null);
	}

	/**
	 * Returns recursively all files which are under {@code directory} for which the
	 * given filter applies.
	 * <p>
	 * If the given filter is {@code null}, all files will be returned.
	 *
	 * @param directory The parent directory from which the recursive search for
	 *                  files will start.
	 * @param filter    The filter which applies to all returned files. This my be
	 *                  {@code null}.
	 * @return A list of all files under the given directory.
	 * @throws IllegalArgumentException if {@code directory} is not a directory.
	 * @throws NullPointerException     if {@code directory} is {@code null}.
	 */
	public static List<File> getFiles(final File directory, final FilenameFilter filter) {
		if (!directory.isDirectory())
			throw new IllegalArgumentException("directory has to be a valid directoy");

		final ArrayList<File> results = new ArrayList<>();
		getFilesImpl(directory, filter, results);
		return results;
	}

	private static void getFilesImpl(final File directory, final FilenameFilter filter, final List<File> list) {
		for (final File file : directory.listFiles(filter)) {
			if (file.isDirectory())
				getFilesImpl(file, filter, list);
			else
				list.add(file);
		}
	}

	/**
	 * Creates a new {@link FilenameFilter} that returns {@code true} for every
	 * directory and the result of the given {@code FilenameFilter} for every file.
	 *
	 * @param filter A {@link FilenameFilter} which will be applied for files.
	 * @return A new {@link FilenameFilter} which behaves as described above.
	 */
	public static FilenameFilter recursiveFilenameFiler(final FilenameFilter filter) {
		return (d, n) -> {
			// recursively go into sub directories
			if (new File(d.getAbsolutePath() + File.separator + n).isDirectory())
				return true;

			return filter.accept(d, n);
		};
	}

	/**
	 * Creates a recursive {@link FilenameFilter} using
	 * {@link #recursiveFilenameFiler(FilenameFilter)} where every non-directory
	 * filename has to match the given regex pattern.
	 * <p>
	 * To behaviour to determine a match is the same as
	 * {@link String#matches(String)}.
	 *
	 * @param regex The pattern of the regex that every filename has to match.
	 * @return
	 */
	public static FilenameFilter recursiveFilenameFiler(final String regex) {
		final Pattern pattern = Pattern.compile(regex);
		return recursiveFilenameFiler((d, n) -> pattern.matcher(n).matches());
	}

	/**
	 * Reads the given input stream and returns its contents.
	 *
	 * @param input The input stream to be read.
	 * @return The contents of the input stream.
	 * @throws IOException
	 */
	public static byte[] toByteArray(final InputStream input) throws IOException {
		return ByteStreams.toByteArray(input);
	}

	/**
	 * Re-throws the given exception as a {@link RuntimeException}.
	 *
	 * @param e The exception to be re-thrown.
	 * @return This method will not actually return anything. This is only so you
	 *         can call the method as follows to tell the Java compiler's flow
	 *         analysis that an exception will be thrown:
	 *         <p>
	 *         {@code throw throwRuntime(someException);}
	 * @throws RuntimeException
	 */
	public static RuntimeException throwRuntime(final Exception e) throws RuntimeException {
		throw new RuntimeException(null, e);
	}

}
