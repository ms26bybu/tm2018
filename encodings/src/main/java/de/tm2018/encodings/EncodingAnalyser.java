package de.tm2018.encodings;

import static java.lang.System.currentTimeMillis;
import static java.util.Objects.requireNonNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import de.tm2018.encodings.detector.WebRecordEncodingDetector;
import de.tm2018.encodings.util.SamplingTimer;

public class EncodingAnalyser implements WebRecordEncodingAnalyser, WebRecordEncodingAnalyserSupplier {

	private final Map<String, WebRecordEncodingDetector> encodingDetectors = new LinkedHashMap<>();

	private SamplingTimer[] timers = new SamplingTimer[0];
	/**
	 * The number of records analysed after which the timings for the
	 * {@link WebRecordEncodingDetector} objects will be printed.
	 * <p>
	 * If this is less than 1, then automatic output will be disabled.
	 */
	public int autoTimingPrintInterval = 1000;
	long samplingCounter = 0;
	long lastPrintTime = currentTimeMillis();

	@Override
	public List<String> getNames() {
		return new ArrayList<>(encodingDetectors.keySet());
	}

	public void addDetector(final String name, final WebRecordEncodingDetector detector) {
		requireNonNull(name);
		requireNonNull(detector);

		if (encodingDetectors.containsKey(name))
			throw new IllegalArgumentException("There is already an encoding detector with the name '" + name + "'.");

		encodingDetectors.put(name, detector);

		// inefficient but we will only do this a few times
		timers = Arrays.copyOf(timers, timers.length + 1);
		timers[timers.length - 1] = new SamplingTimer();
	}

	@Override
	public void analyse(final WebRecord record, final Collection<String> results) {
		int i = 0;
		for (final WebRecordEncodingDetector detector : encodingDetectors.values()) {
			timers[i++].measur(() -> {
				final String encoding = detector.getEncoding(record);
				results.add(encoding);
			});
		}

		// timings
		if (autoTimingPrintInterval > 0 && samplingCounter++ >= autoTimingPrintInterval) {
			samplingCounter = 0;
			System.out.println(
					(currentTimeMillis() - lastPrintTime) * 0.001 + "s for " + autoTimingPrintInterval + " records");
			lastPrintTime = currentTimeMillis();
			printTimings();
		}
	}

	@Override
	public void close() {
		// no-op
	}

	/**
	 * In this case, it returns itself.
	 */
	@Override
	public WebRecordEncodingAnalyser supplyAnalyser() {
		return this;
	}

	public void printTimings() {
		int i = 0;
		for (final Entry<String, WebRecordEncodingDetector> entry : encodingDetectors.entrySet()) {
			System.out.println(entry.getKey() + ": " + timers[i++].getEllapsedMilliSeconds() + "ms");
		}
	}

}
