package de.tm2018.encodings;

import static de.tm2018.encodings.util.Util.webRecordReaderFromFile;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Objects.requireNonNull;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import de.siegmar.fastcsv.writer.CsvAppender;
import de.siegmar.fastcsv.writer.CsvWriter;
import de.tm2018.encodings.util.UpperCaseStringReuser;

public class ArchiveAnalysisWriter implements AutoCloseable {

	private final File archive;
	private final WebRecordEncodingAnalyserSupplier analyserSupplier;
	private final CsvWriter csvWriter = createDefaultCsvWriter();

	private final UpperCaseStringReuser reuser = new UpperCaseStringReuser();

	public ArchiveAnalysisWriter(final String archive, final WebRecordEncodingAnalyserSupplier analyserSupplier) {
		this.archive = new File(requireNonNull(archive));
		this.analyserSupplier = requireNonNull(analyserSupplier);
	}

	private static CsvWriter createDefaultCsvWriter() {
		final CsvWriter writer = new CsvWriter();

		writer.setLineDelimiter(new char[] { '\n' });
		writer.setAlwaysDelimitText(true);

		return writer;
	}

	public String getArchive() {
		return archive.getAbsolutePath();
	}

	public WebRecordEncodingAnalyserSupplier getAnalyserSupplier() {
		return analyserSupplier;
	}

	public CsvWriter getCsvWriter() {
		return csvWriter;
	}

	@Override
	public void close() {
		// no-op
	}

	/**
	 * This is equivalent to {@link #write(CsvWebRecordEncodingAnalyserAppender)}
	 * with {@code appendMethod} equal to {@code null}.
	 *
	 * @throws Exception
	 */
	public void write() throws Exception {
		write(null);
	}

	/**
	 * Analyses the web-archive and writes the result into a CSV file.
	 * <p>
	 * By default, the result file of previous analysis of the same archive are
	 * overwritten. Use {@code appendMethod} to return a new
	 * {@link WebRecordEncodingAnalyserSupplier} for a given {@link File} and
	 * existing supplier. Leave {@code appendMethod} {@code null} to use the default
	 * behaviour.
	 *
	 * @param appendMethod An function returning a new
	 *                     {@link WebRecordEncodingAnalyserSupplier}. This may be
	 *                     {@code null}.
	 * @throws Exception
	 */
	public void write(final CsvWebRecordEncodingAnalyserAppender appendMethod) throws Exception {
		final File csvFile = new File(getCsvPath(archive));
		final File tempFile = new File(csvFile.getAbsolutePath() + ".tmp");

		// delete temporary file
		Files.deleteIfExists(tempFile.toPath());

		try {
			// get supplier
			WebRecordEncodingAnalyserSupplier supplier = analyserSupplier;
			if (appendMethod != null) {
				supplier = appendMethod.append(csvFile, supplier);
			}

			// analyse and write to CSV
			writeToCsv(tempFile, supplier);

			// delete existing file
			Files.deleteIfExists(csvFile.toPath());
			// rename temporary file
			if (!tempFile.renameTo(csvFile)) {
				throw new IOException("Cannot rename temp file.");
			}
		} finally {
			// delete temporary file
			Files.deleteIfExists(tempFile.toPath());
		}
	}

	private WebRecordReader createReader() throws IOException {
		return webRecordReaderFromFile(archive);
	}

	private void writeToCsv(final File csv, final WebRecordEncodingAnalyserSupplier analyserSupplier) throws Exception {
		try (final WebRecordReader reader = createReader();
				final CsvAppender appender = csvWriter.append(csv, UTF_8);
				final WebRecordEncodingAnalyser analyser = analyserSupplier.supplyAnalyser()) {

			// write header
			appender.appendField("URL");
			appender.appendField("Date");

			final List<String> detectorNames = analyser.getNames();
			for (final String detectorName : detectorNames)
				appender.appendField(detectorName);
			appender.endLine();

			// go through records
			final ArrayList<String> tempList = new ArrayList<>(detectorNames.size());

			for (final WebRecord r : reader.sequential()) {
				tempList.clear();
				analyser.analyse(r, tempList);

				// write URL
				appender.appendField(r.url);
				appender.appendField(String.valueOf(r.date.toEpochMilli()));

				// write encodings
				for (final String enc : tempList)
					appender.appendField(reuser.toUpperCase(enc));

				appender.endLine();
			}
		}
	}

	private static final String[] ARCHIVE_EXTENSIONS = new String[] { ".warc.gz", ".warc" };

	/**
	 * Returns the path the resulting CSV file will have given a certain
	 * {@code .warc} or {@code .warc.gz} file.
	 *
	 * @param archive The web-archive file.
	 * @return The path of the resulting CSV file.
	 */
	private static String getCsvPath(final File archive) {
		String arch = archive.getAbsolutePath();

		for (final String extension : ARCHIVE_EXTENSIONS) {
			if (arch.toLowerCase().endsWith(extension)) {
				arch = arch.substring(0, arch.length() - extension.length());
				break;
			}
		}

		return arch + ".csv";
	}

}
