package de.tm2018.encodings;

import java.util.Collection;
import java.util.List;

/**
 * A closable analyser of {@link WebRecord} objects.
 *
 * @author Michael Schmidt
 *
 */
public interface WebRecordEncodingAnalyser extends AutoCloseable {

	/**
	 * Returns a list of names which correspond to the the methods used to extract
	 * encodings from a given record.
	 * <p>
	 * The list and its items are guaranteed to be non-null.
	 *
	 * @return An immutable list of names.
	 */
	List<String> getNames();

	/**
	 * Analyses the given {@link WebRecord} and adds the results to the given
	 * collection in order.
	 * <p>
	 * It is guaranteed that exactly as many items are added to the given collection
	 * as {@code #getNames()} returns.
	 *
	 * @param record  The record to be analysed.
	 * @param results The collection in which the results will be added.
	 * @throws NullPointerException if {@code record} or {@code results} is
	 *                              {@code null}.
	 */
	void analyse(WebRecord record, Collection<String> reults);

}
