package de.tm2018.encodings;

import static java.util.Objects.requireNonNull;

import java.time.Instant;

import org.jwat.common.HttpHeader;
import org.jwat.warc.WarcRecord;

/**
 * A simplified version of a response {@link WarcRecord} describing an HTML web
 * page.
 *
 * @author Michael Schmidt
 *
 */
public class WebRecord {

	/**
	 * The URL of the web record.
	 * <p>
	 * This is guaranteed to be not {@code null}.
	 */
	public final String url;
	/**
	 * The date at which the web record was recorded.
	 * <p>
	 * This is guaranteed to be not {@code null}.
	 */
	public final Instant date;
	/**
	 * The HTTP response header of the web record.
	 * <p>
	 * This is guaranteed to be not {@code null}.
	 */
	public final HttpHeader header;
	/**
	 * The binary content of the web record.
	 * <p>
	 * This is guaranteed to be not {@code null}.
	 */
	public final byte[] content;

	public WebRecord(final String url, final Instant date, final HttpHeader header, final byte[] content) {
		this.url = requireNonNull(url);
		this.date = requireNonNull(date);
		this.header = requireNonNull(header);
		this.content = requireNonNull(content);
	}

}
