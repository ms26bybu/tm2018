package de.tm2018.encodings.detector;

import static java.util.Arrays.asList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import de.tm2018.encodings.util.UpperCaseStringReuser;

public final class EncodingSanitiser {

	private static final EncodingSanitiser INSTANCE = new EncodingSanitiser();

	private final Map<String, String> encodingAliasMap = new HashMap<>();
	private final UpperCaseStringReuser reuser = new UpperCaseStringReuser();

	private EncodingSanitiser() {
		for (final String e : "UTF-8 UTF-7 UTF-16LE UTF-16BE".split(" "))
			addAliases(e);

		// e.g. ISO-8859-1
		for (int i = 1; i <= 16; i++)
			addAliases("ISO-8859-" + i);
		addAliases("LATIN-1", "ISO-8859-1");

		// e.g. WINDOWS-1250
		// all WINDOWS-XXXX can be written as CP-XXXX or WIN-XXXX
		for (int i = 1250; i <= 1257; i++) {
			final String win = "WINDOWS-" + i;
			addAliases(win);
			addAliases("CP-" + i, win);
			addAliases("WIN-" + i, win);
		}

		// TIS-620
		// This is basically ISO-8859-11
		addAliases("TIS-620");

		// EUC
		addAliases("EUC-CN");
		addAliases("EUC-JP");
		addAliases("EUC-KR");
		addAliases("EUC-TW");

		// SHIFT_JIS
		addAliases("SHIFT-JIS", "SHIFT_JIS");
		addAliases("SJIS", "SHIFT_JIS");
	}

	private void addAliases(final String enc) {
		addAliases(enc, enc);
	}

	private void addAliases(final String enc, final String originalEnc) {

		Collection<String> aliases = asList(enc);

		aliases = Util.mapMany(aliases, a -> Util.replaceWithMany(a, '-', asList("", "-", "_", " ", "=", ".")));

		// add aliases to map
		for (final String alias : aliases)
			addAlias(alias, originalEnc);
	}

	private void addAlias(final String alias, final String encoding) {
		encodingAliasMap.put(reuser.toUpperCase(alias), reuser.toUpperCase(encoding));
	}

	private String sanitiseEncoding(String enc) {
		if (enc == null)
			return null;

		enc = enc.trim();

		// remove leading and trailing quotes
		while (!enc.isEmpty() && Util.isQuote(enc.charAt(0)))
			enc = enc.substring(1);
		while (!enc.isEmpty() && Util.isQuote(enc.charAt(enc.length() - 1)))
			enc = enc.substring(0, enc.length() - 1);

		if (enc.isEmpty())
			return null;

		enc = reuser.toUpperCase(enc);

		final String aliased = encodingAliasMap.get(enc);
		if (aliased != null)
			return aliased;

		return enc;
	}

	public static String sanitise(final String encoding) {
		return INSTANCE.sanitiseEncoding(encoding);
	}

	private static class Util {

		public static <T, R> Collection<R> mapMany(final Collection<T> collection,
				final Function<T, Collection<R>> mapper) {
			final ArrayList<R> res = new ArrayList<>();
			for (final T item : collection)
				res.addAll(mapper.apply(item));
			return res;
		}

		/**
		 * Replaces all occurrences of the given character in the given string with all
		 * possible combinations of the given replacements.
		 * <p>
		 * This will returns a collection with {@code pow(replacements.size(), o)} items
		 * where {@code o} is the number of occurrences of the given character in the
		 * given string.
		 *
		 * @param base
		 * @param c
		 * @param replacements
		 * @return
		 */
		public static Collection<String> replaceWithMany(final String base, final char c,
				final Collection<String> replacements) {
			StringBuilder[] builders = new StringBuilder[1];
			builders[0] = new StringBuilder();

			for (int i = 0; i < base.length(); i++) {
				final char current = base.charAt(i);

				if (current != c) {
					for (final StringBuilder builder : builders)
						builder.append(current);
				} else {
					final StringBuilder[] newBuilders = new StringBuilder[builders.length * replacements.size()];
					for (int j = 0; j < builders.length; j++) {
						int k = 0;
						for (final String replacement : replacements)
							newBuilders[j * replacements.size() + k++] = new StringBuilder(builders[j].toString())
									.append(replacement);
					}
					builders = newBuilders;
				}
			}

			// convert and return
			final ArrayList<String> result = new ArrayList<>(builders.length);
			for (final StringBuilder builder : builders)
				result.add(builder.toString());
			return result;
		}

		private static final char[] QUOTES = new char[] { '\'', '"', '\u2033', '\u201d' };

		/**
		 * Returns whether the given character is a quote.
		 *
		 * @param c
		 * @return
		 */
		public static boolean isQuote(final char c) {
			for (final char q : QUOTES)
				if (q == c)
					return true;

			return false;
		}

	}

}
