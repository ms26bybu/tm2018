package de.tm2018.encodings.detector;

import static java.util.regex.Pattern.CASE_INSENSITIVE;
import static java.util.regex.Pattern.compile;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jwat.common.HeaderLine;
import org.jwat.common.HttpHeader;

import de.tm2018.encodings.WebRecord;

/**
 * A {@link WebRecordEncodingDetector} which uses the {@code Content-Type} of
 * the HTTP response header of a {@link WebRecord} to determine the encoding of
 * the record's content.
 *
 * @author Michael Schmidt
 *
 */
public class HttpHeaderEncodingDetector implements WebRecordEncodingDetector {

	private static final Pattern HEADER_CHARSET_PATTERN = compile(";\\s*charset\\s*=\\s*([^;]+)", CASE_INSENSITIVE);

	@Override
	public String getEncoding(final WebRecord record) {
		if (record.header == null)
			return null;

		return getEncodingFromHeader(record.header);
	}

	/**
	 * Returns the encoding described in the given {@link HttpHeader} or
	 * {@code null}.
	 *
	 * @param header The HTTP header.
	 * @return The encoding or {@code null}.
	 * @throws NullPointerException If {@code header} is {@code null}.
	 */
	public static String getEncodingFromHeader(final HttpHeader header) {
		// getHeader(String) ignores case
		final HeaderLine line = header.getHeader("content-type");

		// no content-type
		if (line == null)
			return null;

		final Matcher matcher = HEADER_CHARSET_PATTERN.matcher(line.value);

		// cannot match pattern
		if (!matcher.find())
			return null;

		final String encoding = matcher.group(1).trim();

		// null or empty
		if (encoding == null || encoding.isEmpty())
			return null;

		// ignore HTTP placeholders
		if ("*".equals(encoding))
			return null;

		return encoding;
	}
}
