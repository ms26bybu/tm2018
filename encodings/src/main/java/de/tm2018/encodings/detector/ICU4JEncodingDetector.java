package de.tm2018.encodings.detector;

import com.ibm.icu.text.CharsetDetector;
import com.ibm.icu.text.CharsetMatch;

import de.tm2018.encodings.WebRecord;

public class ICU4JEncodingDetector implements WebRecordEncodingDetector {

	public static final int DEFAULT_CONFIDENCE_THESHOLD = 40;

	private final CharsetDetector detector = new CharsetDetector();
	private final int confidenceThreshold;

	public ICU4JEncodingDetector() {
		this(DEFAULT_CONFIDENCE_THESHOLD);
	}

	public ICU4JEncodingDetector(final int confidenceThreshold) {
		this.confidenceThreshold = confidenceThreshold;
	}

	@Override
	public String getEncoding(final WebRecord record) {
		if (record.content == null)
			return null;

		detector.setText(record.content);
		final CharsetMatch match = detector.detect();

		if (match == null || match.getConfidence() < confidenceThreshold)
			return null;

		return match.getName();
	}

}
