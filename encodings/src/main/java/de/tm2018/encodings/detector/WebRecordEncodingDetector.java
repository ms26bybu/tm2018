package de.tm2018.encodings.detector;

import de.tm2018.encodings.WebRecord;
import de.uni_leipzig.asv.tools.jwarcex.encoding_detection.EncodingDetector;

/**
 * A detector of the encoding of the content of a {@link WebRecord}.
 *
 * @see EncodingDetector
 * @author Michael Schmidt
 *
 */
public interface WebRecordEncodingDetector {

	/**
	 * Returns the encoding of the content of the given {@link WebRecord} or
	 * {@code null}.
	 *
	 *
	 * @param record The record to be analyzed for the encoding of its content.
	 * @return The encoding of the record's content of {@code null} if no encoding
	 *         was detected.
	 * @throws NullPointerException If {@code record} is {@code null}.
	 */
	String getEncoding(final WebRecord record);

	/**
	 * Adapts a given {@link EncodingDetector} returning a
	 * {@link WebRecordEncodingDetector} which calls
	 * {@link EncodingDetector#getEncoding(byte[])} on the content of a
	 * {@link WebRecord}.
	 *
	 * @param detector The {@code EncodingDetector} to be adapted.
	 * @return The adapted detector or {@code null} if {@code detector} is
	 *         {@code null}.
	 */
	static WebRecordEncodingDetector adapt(final EncodingDetector detector) {
		if (detector == null)
			return null;

		return r -> detector.getEncoding(r.content);
	}

	static WebRecordEncodingDetector sanitise(final WebRecordEncodingDetector detector) {
		if (detector == null)
			return null;

		return r -> EncodingSanitiser.sanitise(detector.getEncoding(r));
	}
}
