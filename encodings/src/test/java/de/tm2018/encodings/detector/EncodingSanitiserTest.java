package de.tm2018.encodings.detector;

import static de.tm2018.encodings.detector.EncodingSanitiser.sanitise;
import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import org.junit.Test;

public class EncodingSanitiserTest {

	@Test
	public void testSanitise() {
		final Map<String, Collection<String>> testMap = new LinkedHashMap<>();

		testMap.put(null, asList(null, "", "  ", "''")); // do NOT test null here
		testMap.put("UTF-8", asList("UTF-8", "UTF_8", "'UTF-8'", "UTF8"));

		testMap.forEach((expected, testCases) -> {
			for (final String testCase : testCases) {
				assertEquals(expected, sanitise(testCase));
			}
		});
	}

}
