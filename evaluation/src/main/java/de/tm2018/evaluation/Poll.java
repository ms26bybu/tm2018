package de.tm2018.evaluation;

import java.util.HashMap;

public class Poll<T> {

	private final HashMap<T, Long> ballotBox = new HashMap<>();

	public void vote(final T candidate) {
		vote(candidate, 1);
	}

	public void vote(final T candidate, final long amount) {
		ballotBox.merge(candidate, amount, (a, b) -> a + b);
	}

	public void clear() {
		ballotBox.clear();
	}

	public T getWinner(final ElectionRules<T> electionRules) {
		return electionRules.getWinner(ballotBox);
	}

}
