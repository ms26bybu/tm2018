package de.tm2018.evaluation.io;

import java.util.ArrayList;
import java.util.List;

public class Categories {

	private final List<Category> categories = new ArrayList<>();

	public Categories() {
	}

	/**
	 * Returns a List of Directory objects.
	 *
	 * @return a List object.
	 */
	public List<Category> getAll() {
		return categories;
	}

	/**
	 * Adds a new Directory object, with the given directory name to the List.
	 *
	 * @param dirName a String value.
	 */
	public void add(final String dirName) {
		categories.add(new Category(dirName));
	}

	/**
	 * Returns the Directory with the given name if there is no such directory null
	 * is returned.
	 *
	 * @param dirName a String value.
	 * @return a Directory object or null.
	 */
	public Category get(final String dirName) {
		for (final var dir : categories) {
			if (dirName.equals(dir.getName())) {
				return dir;
			}
		}

		return null;
	}

	@Override
	public String toString() {
		final StringBuilder sBuilder = new StringBuilder();

		for (final var cat : categories) {
			sBuilder.append(cat.getName()).append("\n");
		}

		return sBuilder.toString();
	}

}
