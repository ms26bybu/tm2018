package de.tm2018.evaluation.io;

import de.tm2018.evaluation.data.LanguageMap;

public class Category {

	private final String name;
	private final LanguageMap langMap;

	public Category(final String dirName) {
		this.name = dirName;
		langMap = new LanguageMap();
	}

	/**
	 * Returns the name of the directory.
	 *
	 * @return a String value.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Adds a new language to the LanguageMap, using the add method form
	 * LanguageMap.
	 *
	 * @param language a String value.
	 * @param encoding a String value.
	 */
	public void addLanguage(final String language, final String encoding) {
		langMap.add(language, encoding);
	}

	/**
	 * Returns the LanguageMap generated for this directory.
	 *
	 * @return a LanguageMap object.
	 */
	public LanguageMap getLanguageMap() {
		return langMap;
	}
}
