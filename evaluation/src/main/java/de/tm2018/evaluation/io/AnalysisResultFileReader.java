package de.tm2018.evaluation.io;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;

import de.tm2018.encodings.ArchiveAnalysisReader;
import de.tm2018.evaluation.ElectionRules;
import de.tm2018.evaluation.Poll;
import de.tm2018.evaluation.data.EncodingCounter;
import de.tm2018.evaluation.data.Language;

public class AnalysisResultFileReader {

	private final HashMap<String, EncodingCounter> encodingCountsAbs = new HashMap<>();
	private final Categories directories = new Categories();
	private final ElectionRules<String> rules;
	private final Poll<String> poll = new Poll<>();
	private final HashMap<String, HashSet<String>> allEncodingsPerDirectory = new HashMap<>();

	public static final String UNKNOWN_LABEL = "unknown";

	public AnalysisResultFileReader(final ElectionRules<String> rules) {
		this.rules = rules;
	}

	/**
	 * Iterates through the subelements of a directory and calls readFile for every
	 * of them.
	 *
	 * @param fdir a File object.
	 */
	private void readDir(final File fdir) {
		final var ldir = fdir.list();
		directories.add(fdir.getName());

		for (final var file : ldir) {
			readFile(fdir + "\\" + file);
		}
	}

	/**
	 * Iterates through the given file, if the file is an directory readDir is
	 * called.
	 *
	 * @param file a String value.
	 */
	public void readFile(final String file) {

		final File fdir = new File(file);
		if (fdir.isDirectory()) {
			readDir(fdir);
		} else {
			try (var reader = new ArchiveAnalysisReader(fdir)) {
				// the names of the detectors
				// basically the header of the CSV without 'URL' and 'Date'
				final var detectorName = new ArrayList<String>();

				// And Iterable<AnalysisResult> which will read the CSV as go.
				final var iter = reader.read(detectorName::addAll);

				var i = 0;

				final Category directory = directories.get(new File(fdir.getParent()).getName());
				final Language language = new Language(fdir.getName().split("_")[0]);
				directory.getLanguageMap().insertLang(language);

				// iterate throw the CSV file
				// This might throw if the CSV is not formatted correctly.
				for (final var row : iter) {
					i++;

					final String winner = getWinner(row.getEncodings(), false);

					if (!UNKNOWN_LABEL.equals(winner)) {
						allEncodingsPerDirectory.get(parentDirectory).add(winner);

						encodingCountsAbs.computeIfAbsent(winner, EncodingCounter::new).increase();

						directory.getLanguageMap().updateEncodingCount(language, winner);
					}
				}

				directory.getLanguageMap().setTotalEntries(i, language);

				// Note: using this approach you don't have to read the whole file at once.
				// You can stream through it. This is more memory efficient.

				// Note: You can only iterate the CSV file once!
				// If you need to iterate it twice, make a new reader or add all results into a
				// collection.

			} catch (final IOException e) {
				e.printStackTrace();
			}
		}
	}

	private String getWinner(final Collection<String> encodings, final boolean allowNullValues) {
		for (final var enc : encodings) {
			if (allowNullValues || enc != null) {
				poll.vote(enc);
			}
		}

		final var winner = poll.getWinner(rules);
		poll.clear();

		if (winner == null) {
			return UNKNOWN_LABEL;
		}
		return winner;
	}

	/**
	 * predicts the encoding, ignores null values, but the detected encoding has to
	 * be detected more than 2 times
	 *
	 * @param encodings
	 * @return
	 */
	public String predictEncodingNoNull(final ArrayList<String> encodings) {
		final var encodingCounts = new LinkedHashMap<String, Integer>();
		int noDet = 0;
		for (var i = 0; i < encodings.size(); i++) {
			if (encodings.get(i) == null) {
				noDet = Collections.frequency(encodings, encodings.get(i));
			} else {
				encodingCounts.putIfAbsent(String.valueOf(encodings.get(i)),
						Collections.frequency(encodings, encodings.get(i)));
			}
		}

		final int div = (encodings.size() - noDet) / 2;

		for (final var enc : encodingCounts.entrySet()) {
			if (enc.getValue() > div && enc.getValue() > 2) {
				return enc.getKey();
			}
		}

		return "unknown";
	}

	/**
	 * Returns the Directories object.
	 *
	 * @return a Directories object.
	 */
	public Categories getDirectories() {
		return directories;
	}
	
	public HashMap<String, HashSet<String>> getAllEncodingsPerDirectory() {
		return allEncodingsPerDirectory;
	}

}
