package de.tm2018.evaluation.util;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;

public class Images {

	private Images() {
	}

	public static void saveImage(final Image image, final String path) {
		final var file = new File(path);
		file.mkdirs();

		// get the image format from the file name
		final var parts = file.getName().split("\\.");
		final var extension = parts[parts.length - 1].toLowerCase();

		final BufferedImage bImage = SwingFXUtils.fromFXImage(image, null);
		try {
			ImageIO.write(bImage, extension, file);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}

}
