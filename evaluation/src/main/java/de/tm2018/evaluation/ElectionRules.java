package de.tm2018.evaluation;

import java.util.Map;

/**
 * The rules of an election.
 * <p>
 * This controls who the winner is.
 *
 * @author Michael
 *
 * @param <T>
 */
@FunctionalInterface
public interface ElectionRules<T> {

	/**
	 * Returns the winner of the election.
	 *
	 * @param ballotBox
	 * @return The winner of the election or {@code null}, if the election was not
	 *         conclusive.
	 */
	T getWinner(Map<T, Long> ballotBox);

	/**
	 * Returns the winner of an absolute majority.
	 * <p>
	 * This only makes sense if the required percentage is greater than 50%.
	 *
	 * @param percentage The requirement to win. This has to be in the interval
	 *                   {@code (0.5, 1]}.
	 * @param minVotes
	 * @return
	 */
	static <T> ElectionRules<T> absoluteMajority(final double percentage, final long minVotes) {
		return ballotBox -> {
			// the total number of votes
			final long total = ballotBox.values().stream().reduce(0L, (a, b) -> a + b);

			// number of votes necessary to win
			final long requirementToWin = (long) Math.ceil(total * percentage);

			// not enough votes
			if (total < minVotes)
				return null;

			for (final var entry : ballotBox.entrySet()) {
				if (entry.getValue() >= requirementToWin)
					return entry.getKey();
			}

			// no absolute majority
			return null;
		};
	}

	static <T> ElectionRules<T> winnerTakesItAll(final long minVotes) {
		return ballotBox -> {
			long total = 0;

			long maxVotes = Long.MIN_VALUE;
			T maxCandidate = null;

			for (final var entry : ballotBox.entrySet()) {
				final long votes = entry.getValue();
				final T candidate = entry.getKey();

				total += votes;

				if (votes == maxVotes) {
					maxCandidate = null;
				} else if (votes > maxVotes) {
					maxVotes = votes;
					maxCandidate = candidate;
				}
			}

			if (total < minVotes)
				return null;

			return maxCandidate;
		};
	}

}
