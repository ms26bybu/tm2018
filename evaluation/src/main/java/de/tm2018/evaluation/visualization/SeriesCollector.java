package de.tm2018.evaluation.visualization;

import java.util.ArrayList;
import java.util.List;

import javafx.scene.chart.XYChart.Series;

public class SeriesCollector {

	private final List<Series<String, Number>> dataSeriesVer = new ArrayList<>();
	private final List<Series<Number, String>> dataSeriesHor = new ArrayList<>();
	private Number maxValue = 0;

	public SeriesCollector() {

	}

	public SeriesCollector(final DataCollector dataCollector) {
		addDataSeries(dataCollector);
	}

	/**
	 * Returns a List of Series objects, initialized as (String, Number) pairs.
	 *
	 * @return a List object.
	 */
	public List<Series<String, Number>> getVerticalSeries() {
		return dataSeriesVer;
	}

	/**
	 * Returns a List of Series objects, initialized as (Number, String) pairs.
	 *
	 * @return a List object.
	 */
	public List<Series<Number, String>> getHorizontalSeries() {
		return dataSeriesHor;
	}

	/**
	 * Returns the max Value of all stored Series, if the object is empty the value
	 * is 0.
	 *
	 * @return a Number object
	 */
	public Number getMaxValue() {
		return maxValue;
	}

	/**
	 * Gets a DataCollector object and adds its Series to the corresponding List.
	 *
	 * The max value is assigned here as well, by the maximum numeric value in the
	 * DataCollector if it is not already bigger.
	 *
	 * @param dataCollector a DataCollector object
	 */
	public void addDataSeries(final DataCollector dataCollector) {
		dataCollector.sortData();
		dataSeriesVer.add(dataCollector.getVerticalSeries());
		dataSeriesHor.add(dataCollector.getHorizontalSeries());

		for (final var data : dataCollector.getDatasVer()) {
			if (maxValue.doubleValue() < data.getYValue().doubleValue()) {
				maxValue = data.getYValue();
			}
		}
	}

}
