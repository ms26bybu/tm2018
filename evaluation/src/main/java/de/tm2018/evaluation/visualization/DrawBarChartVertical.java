package de.tm2018.evaluation.visualization;

import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;

public class DrawBarChartVertical implements DrawBarChart {

	private final CategoryAxis categoryAxis = new CategoryAxis();
	private final NumberAxis numberAxis = new NumberAxis();
	private final BarChart<String, Number> bChart = new BarChart<>(categoryAxis, numberAxis);

	public DrawBarChartVertical(final String bChartTitle) {
		bChart.setTitle(bChartTitle);
		bChart.setLegendVisible(false);

	}

	@Override
	public void addSeries(final SeriesCollector seriesCollector) {
		bChart.getData().addAll(seriesCollector.getVerticalSeries());

		// calculates the number axis scale in a way the labels should not be cut.
		calcTickUnitsAndUpperBound(seriesCollector);
	}

	/**
	 * Calculates the tick units and the upper bounding, to prevent cutting the
	 * labels in top of the charts to be cut by the edges.
	 *
	 * @param seriesCollector a SeriesCollector object.
	 */
	private void calcTickUnitsAndUpperBound(final SeriesCollector seriesCollector) {
		final var max = seriesCollector.getMaxValue().doubleValue();

		if (seriesCollector.getVerticalSeries().get(0).getData().get(0).getYValue() instanceof Double) {
			numberAxis.setAutoRanging(false);
			final var bound = Math.ceil((max + 5) / 10) * 10;
			numberAxis.setUpperBound(bound);
		} else {
			numberAxis.setAutoRanging(false);

			final var div = getDiv(max);
			numberAxis.setTickUnit(max < 10 ? 1 : div < 10000 ? div / 2 : div);

			final var bound = max < 10 ? Math.ceil(max * 1.1) : Math.ceil(max * 1.1 / div) * div;
			numberAxis.setUpperBound(bound);
		}
	}

	private double getDiv(final double max) {
		double div = 10;

		while (max / div > 10) {
			div *= 10;
		}

		return div;
	}

	@Override
	public void showLegend(final boolean sLegend) {
		bChart.setLegendVisible(sLegend);
	}

	@Override
	public void setXAxisLabel(final String xAxisLabel) {
		categoryAxis.setLabel(xAxisLabel);
	}

	@Override
	public void setYAxisLabel(final String yAxisLabel) {
		numberAxis.setLabel(yAxisLabel);
	}

	@Override
	public Scene getBarChartScene(final int width, final int height) {
		return new Scene(bChart, width, height);
	}

	public void setCategoryAxisLabelRotation(final double rot) {
		categoryAxis.setTickLabelRotation(rot);
	}

}
