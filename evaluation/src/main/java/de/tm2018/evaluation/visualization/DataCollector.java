package de.tm2018.evaluation.visualization;

import java.util.ArrayList;
import java.util.List;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;

public class DataCollector {

	private final List<Data<String, Number>> datasVer = new ArrayList<>();
	private final List<Data<Number, String>> datasHor = new ArrayList<>();

	public DataCollector() {

	}

	/**
	 * Returns a List object, containing Data objects as (String, Number) pairs.
	 *
	 * @return a List object.
	 */
	public List<Data<String, Number>> getDatasVer() {
		return datasVer;
	}

	/**
	 * Returns a List object, containing Data objects as (Number, String) pairs.
	 *
	 * @return a List object.
	 */
	public List<Data<Number, String>> getDatasHor() {
		return datasHor;
	}

	/**
	 * Adds a new Data object to both Lists.
	 *
	 * @param xValue a String value.
	 * @param yValue a Number value.
	 */
	public void addNewData(final String xValue, final Number yValue) {
		datasVer.add(new Data<>(xValue, yValue));
		datasHor.add(new Data<>(yValue, xValue));
	}

	/**
	 * Returns a Series object with (String, Number) pairs.
	 *
	 * @return a Series object.
	 */
	public Series<String, Number> getVerticalSeries() {
		final Series<String, Number> series = new Series<>();
		series.getData().addAll(datasVer);

		return series;
	}

	/**
	 * Returns a Series object with (Number, String) pairs.
	 *
	 * @return a Series object.
	 */
	public Series<Number, String> getHorizontalSeries() {
		final Series<Number, String> series = new Series<>();
		series.getData().addAll(datasHor);

		return series;
	}

	/**
	 * Adds Labels containing the numerical values of the Data object.
	 */
	public void addLabels() {
		String labelText;
		for (final var data : datasVer) {
			if (data.getYValue() instanceof Double) {
				labelText = String.format("%.3f %%", data.getYValue());
			} else {
				labelText = data.getYValue().toString();
			}

			data.setNode(getNodeVer(labelText));
		}

		for (final var data : datasHor) {
			if (data.getXValue() instanceof Double) {
				labelText = String.format("%.3f %%", data.getXValue());
			} else {
				labelText = data.getXValue().toString();
			}

			data.setNode(getNodeHor(labelText));
		}
	}

	/**
	 * Returns a StackPane object, representing the Label and its position, in a
	 * horizontal bar chart.
	 *
	 * @param labelText a String value.
	 * @return a StackPane object.
	 */
	private StackPane getNodeHor(final String labelText) {
		final StackPane node = new StackPane();
		final Label label = new Label(labelText);
		final Group group = new Group(label);

		// label position
		StackPane.setAlignment(group, Pos.CENTER_RIGHT);
		StackPane.setMargin(group, new Insets(0, 0, 5, 0));

		node.getChildren().add(group);
		node.setPadding(new Insets(-60));

		return node;
	}

	/**
	 * Returns a StackPane object, representing the Label and its position, in a
	 * vertical bar chart.
	 *
	 * @param labelText a String value.
	 * @return a StackPane object.
	 */
	private StackPane getNodeVer(final String labelText) {
		final StackPane node = new StackPane();
		final Label label = new Label(labelText);
		final Group group = new Group(label);

		// label position
		StackPane.setAlignment(group, Pos.BASELINE_CENTER);
		StackPane.setMargin(group, new Insets(0, 0, 5, 0));

		node.getChildren().add(group);
		node.setPadding(new Insets(-5));

		return node;
	}

	/**
	 * Sorts the Data lists by decreasing Number values.
	 */
	public void sortData() {
		datasVer.sort((a, b) -> -Double.compare(a.getYValue().doubleValue(), b.getYValue().doubleValue()));
		datasHor.sort((a, b) -> Double.compare(a.getXValue().doubleValue(), b.getXValue().doubleValue()));
	}

	@Override
	public String toString() {
		final var stringBuilder = new StringBuilder();

		for (final var data : datasVer) {
			stringBuilder.append(data.getXValue()).append(": ").append(data.getYValue());
		}
		stringBuilder.append("\n");

		for (final var data : datasHor) {
			stringBuilder.append(data.getXValue()).append(": ").append(data.getYValue());
		}

		return stringBuilder.toString();

	}

}
