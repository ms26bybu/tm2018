package de.tm2018.evaluation.visualization;

import javafx.scene.Scene;

public interface DrawBarChart {

	/**
	 * Adds the list of series stored in the SeriesCollector object to the BarChart
	 * object.
	 *
	 * @param seriesCollector a SeriesCollector object
	 */
	public void addSeries(SeriesCollector seriesCollector);

	/**
	 * If true a legend is added to the BarChart.
	 *
	 * @param sLegend a boolean value.
	 */
	public void showLegend(boolean sLegend);

	/**
	 * Label the x-axis with the given String.
	 *
	 * @param xAxisLabel a String object.
	 */
	public void setXAxisLabel(String xAxisLabel);

	/**
	 * Label the y-axis with the given String.
	 *
	 * @param yAxisLabel a String object.
	 */
	public void setYAxisLabel(String yAxisLabel);

	/**
	 * Draw the bar chart to a Scene, with given width and height and returns the
	 * Scene object.
	 *
	 * @param width  integer value.
	 * @param height integer value.
	 * @return a Scene object.
	 */
	public Scene getBarChartScene(int width, int height);
}
