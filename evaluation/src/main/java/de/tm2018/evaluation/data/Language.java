package de.tm2018.evaluation.data;

import static java.util.Collections.unmodifiableSet;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Language {

	// represents the language prefix
	private final String langCode;

	// represents the number of all entries corresponding to this language
	private long totalEntries;

	// holds the detected encodings for this language
	private final Map<String, EncodingCounter> encodings = new HashMap<>();

	/**
	 * Creates a {@code Language} object. It is only initialized with the language
	 * prefix.
	 *
	 * @param lang A {@code String} representing the language prefix.
	 */
	public Language(final String lang) {
		langCode = lang;
	}

	/**
	 * Returns the language prefix.
	 *
	 * @return A {@code String}.
	 */
	public String getLang() {
		return langCode;
	}

	/**
	 * Checks if the given encoding is used in this language data.
	 *
	 * @param encoding A {@code String}.
	 * @return A {@code boolean}.
	 */
	public boolean containsEncoding(final String encoding) {
		return encodings.containsKey(encoding);
	}

	/**
	 * Returns the counted occurrences for the given encoding.
	 *
	 * @param encoding A {@code String} representing the encoding.
	 * @return A {@code long}.
	 */
	public long getEncodingOccurensess(final String encoding) {
		return encodings.get(encoding).getCount();
	}

	public Set<String> getEncodingList() {
		return unmodifiableSet(encodings.keySet());
	}

	/**
	 * Returns the number of total entries that correspond to this language prefix.
	 *
	 * @return A {@code long}.
	 */
	public long getTotalEntries() {
		return totalEntries;
	}

	/**
	 * Returns a {@code HashMap} matching an encoding represented as a
	 * {@code String} to an {@code EncodingCounter} object.
	 *
	 * @return A {@code HashMap}.
	 */
	public Map<String, EncodingCounter> getEncodings() {
		return encodings;
	}

	/**
	 * Increases {@value totalEntries} by the given value.
	 *
	 * @param entryCount A {@code int} value.
	 */
	public void setTotalEntries(final int entryCount) {
		totalEntries += entryCount;
	}

	/**
	 * Inserts a encoding to {@value encoding} if the encoding already exists, the
	 * {@code EncodingCounter} method {@code increaseCount} gets used to increase
	 * the count of the given encoding, else a new Entry will be created.
	 *
	 * @param encoding
	 */
	public void insertEncoding(final String encoding) {
		encodings.computeIfAbsent(encoding, EncodingCounter::new).increase();
	}

	/**
	 * Returns a String used for debugging.
	 */
	@Override
	public String toString() {
		final StringBuilder stringBuilder = new StringBuilder();

		for (final Map.Entry<String, EncodingCounter> i : encodings.entrySet()) {
			stringBuilder.append(i.getKey()).append(": ").append(i.getValue().getCount()).append("\n");
		}
		stringBuilder.append("\n").append("total: ").append(totalEntries).append("\n");

		return stringBuilder.toString();
	}
}
