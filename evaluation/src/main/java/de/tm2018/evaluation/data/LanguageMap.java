package de.tm2018.evaluation.data;

import static java.util.Collections.unmodifiableMap;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class LanguageMap {

	// holds language prefixes and the corresponding language object
	private final Map<String, Language> languages = new HashMap<>();

	/**
	 * Creates a LanguageMap object.
	 */
	public LanguageMap() {
	}

	/**
	 * Returns a {@code HashMap} matching the language prefix as a {@code String}
	 * with a corresponding {@code Language} object.
	 *
	 * @return A Hashmap.
	 */
	public Map<String, Language> getLanguages() {
		return unmodifiableMap(languages);
	}

	/**
	 * Inserts a new entry in the {@code HashMap}.
	 *
	 * @param language A {@code Language} object.
	 */
	public void insertLang(final Language language) {
		languages.putIfAbsent(language.getLang(), language);
	}

	/**
	 * Updates the total entries corresponding to the language.
	 *
	 * @param entryCount The number of entries that match the language prefix.
	 * @param language   The {@code Language} object to update.
	 */
	public void setTotalEntries(final int entryCount, final Language language) {
		languages.get(language.getLang()).setTotalEntries(entryCount);
	}

	/**
	 * Returns all detected encodings. Every detected encoding is included, that is
	 * used by any language.
	 *
	 * @return
	 */
	public Set<String> getAllDetectedEncodings() {
		final var set = new HashSet<String>();
		languages.values().forEach(lang -> set.addAll(lang.getEncodingList()));
		return set;
	}

	/**
	 * Updates the corresponding entry using the {@code insertEncoding} method from
	 * the {@code Language} object.
	 *
	 * @param language A {@code Language} object.
	 * @param encoding A {@code String} object representing the detected encoding.
	 */
	public void updateEncodingCount(final Language language, final String encoding) {
		languages.get(language.getLang()).insertEncoding(encoding);
	}

	public void add(final String language, final String encoding) {
		languages.computeIfAbsent(language, Language::new).insertEncoding(encoding);
	}

	/**
	 * Builds a String used for debugging.
	 */
	@Override
	public String toString() {
		final StringBuilder stringBuilder = new StringBuilder();

		for (final Map.Entry<String, Language> entry : languages.entrySet()) {
			stringBuilder.append(entry.getKey()).append("\n").append(entry.getValue().toString()).append("\n");
		}

		return stringBuilder.toString();
	}
}
