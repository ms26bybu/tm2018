package de.tm2018.evaluation.data;

public class EncodingCounter {

	private final String encoding;
	private long count = 0;

	/**
	 * Constructs an EncodingCounter object, with the given encoding and sets its
	 * count to 1.
	 *
	 * @param encoding a String value.
	 */
	public EncodingCounter(final String encoding) {
		this.encoding = encoding;
	}

	/**
	 * Returns a String value representing the encodings name.
	 *
	 * @return a String value.
	 */
	public String getEncodingName() {
		return encoding;
	}

	/**
	 * Returns the counted occurrences.
	 *
	 * @return a long value.
	 */
	public long getCount() {
		return count;
	}

	/**
	 * Increases the occurrences count by 1.
	 */
	public void increase() {
		count++;
	}

	/**
	 * Increases the occurrences count by the given value.
	 *
	 * @param add a long value.
	 */
	public void increase(final long add) {
		count += add;
	}

}
