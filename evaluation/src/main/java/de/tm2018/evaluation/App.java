package de.tm2018.evaluation;

import static de.tm2018.evaluation.util.Images.saveImage;

import java.util.LinkedHashMap;
import java.util.Map;

import de.tm2018.evaluation.data.EncodingCounter;
import de.tm2018.evaluation.detector.DetectorFileReader;
import de.tm2018.evaluation.io.AnalysisResultFileReader;
import de.tm2018.evaluation.io.Categories;
import de.tm2018.evaluation.visualization.DataCollector;
import de.tm2018.evaluation.visualization.DrawBarChartHorizontal;
import de.tm2018.evaluation.visualization.DrawBarChartVertical;
import de.tm2018.evaluation.visualization.SeriesCollector;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.stage.Stage;

public class App extends Application {

	/**
	 * The path of the directory containing the sub-directories (not recursively)
	 * which contain the CSV files.
	 */
	private static final String PATH_TO_DATA = "D:\\tm\\output\\";
	/**
	 * The path of the directory in which to output the generated diagrams.
	 */
	private static final String SAVE_PATH_DATA = "D:\\tm\\output\\images\\";

	private static final int IMAGE_WIDTH = 1200;
	private static final int IMAGE_HEIGHT = 800;

	public static void main(final String[] args) {
		Application.launch(args);
	}

	@Override
	public void start(final Stage stage) {

		System.out.println("reading data ...");

		final var t1 = System.currentTimeMillis();
		final AnalysisResultFileReader reader = new AnalysisResultFileReader(ElectionRules.absoluteMajority(0.65, 2));
		reader.readFile(PATH_TO_DATA);
		final var t2 = System.currentTimeMillis();

		writeTables(reader);

		System.out.println("file reading time: " + (t2 - t1));
		System.out.println("chart construction ...");

		final var t3 = System.currentTimeMillis();

		// iterate over dirs with data
		drawCharts(reader.getDirectories());

		final var t4 = System.currentTimeMillis();
		System.out.println("construction time: " + (t4 - t3));

		System.out.println("completed");

		System.out.println("overall time consumption: " + (t4 - t1));

		// kills the application thread after the task is done
		Platform.exit();
	}

	@Override
	public void stop() throws Exception {
		super.stop();
	}
	
	public void writeTables(AnalysisResultFileReader reader) {
		final String MARKER = "\"";
		final String SPLITTER = ";";

		for (final var dir : reader.getDirectories().getAll()) {
			if (dir.getLanguageMap().getLanguages().isEmpty()) {
				continue;
			}
			try {
				final var writer = new BufferedWriter(
						new FileWriter(SAVE_PATH_DATA + "tables\\" + dir.getName() + ".csv"));

				var header = "\t";

				final var allDetectedEncodings = reader.getAllEncodingsPerDirectory().get(dir.getName()).stream()
						.toArray(String[]::new);

				for (final var enc : allDetectedEncodings) {
					header += MARKER + enc + MARKER + SPLITTER;
				}
				header += MARKER + "sum" + MARKER + "\n";

				writer.write(header);
				for (final var dirContent : dir.getLanguageMap().getLanguages().entrySet()) {
					var entry = MARKER + dirContent.getKey() + MARKER + SPLITTER;
					var totalEntries = 0;

					for (final var enc : allDetectedEncodings) {
						entry += MARKER + dirContent.getValue().getEncodingOccurences(enc) + MARKER + SPLITTER;
						totalEntries += dirContent.getValue().getEncodingOccurences(enc);
					}

					entry += MARKER + totalEntries + MARKER + "\n";

					writer.write(entry);
					writer.flush();
				}

			} catch (final IOException ioe) {
				ioe.printStackTrace();
			}
		}
	}

	public void drawCharts(final Categories dirs) {
		// iterate over dirs with data
		for (final var dir : dirs.getAll()) {
			if (dir.getLanguageMap().getLanguages().isEmpty()) {
				continue;
			}

			final String path = SAVE_PATH_DATA + dir.getName() + "\\";
			final var overviewMap = new LinkedHashMap<String, EncodingCounter>();

			// iterate over languageMap
			for (final var dirContent : dirs.get(dir.getName()).getLanguageMap().getLanguages().entrySet()) {

				final SeriesCollector sCollector = new SeriesCollector();
				final DataCollector dCollectorRel = new DataCollector();
				final DataCollector dCollectorAbs = new DataCollector();

				// get encoding counts for every detected encoding
				// add them to dataCollector
				for (final var enc : dirContent.getValue().getEncodingList()) {
					dCollectorRel.addNewData(enc, (double) dirContent.getValue().getEncodingOccurensess(enc)
							/ (double) dirContent.getValue().getTotalEntries() * 100);

					dCollectorAbs.addNewData(enc, dirContent.getValue().getEncodingOccurensess(enc));

					overviewMap.computeIfAbsent(enc, EncodingCounter::new)
							.increase(dirContent.getValue().getEncodingOccurensess(enc));
				}

				drawChartNoLabels(dCollectorAbs, path + dirContent.getKey() + "\\abs", dirContent.getKey());
				drawChartNoLabels(dCollectorRel, path + dirContent.getKey() + "\\rel", dirContent.getKey());

				drawChartLabeled(dCollectorAbs, path + dirContent.getKey() + "\\abs", dirContent.getKey());
				drawChartLabeled(dCollectorRel, path + dirContent.getKey() + "\\rel", dirContent.getKey());

				System.gc();
			}

			createAbsOverviewBarChart(overviewMap, path);
			createRelOverviewBarChart(overviewMap, path);

		}
	}

	public void createAbsOverviewBarChart(final Map<String, EncodingCounter> map, final String path) {
		final var absDCollector = new DataCollector();

		for (final var enc : map.entrySet()) {
			absDCollector.addNewData(enc.getKey(), enc.getValue().getCount());
		}

		drawChartNoLabels(absDCollector, path + "\\overview\\abs", "overview");
		drawChartLabeled(absDCollector, path + "\\overview\\abs", "overview");
	}

	public void createRelOverviewBarChart(final Map<String, EncodingCounter> map, final String path) {
		final var absDCollector = new DataCollector();
		double absEntries = 0;

		for (final var enc : map.entrySet()) {
			absEntries += enc.getValue().getCount();
		}

		for (final var enc : map.entrySet()) {
			absDCollector.addNewData(enc.getKey(), enc.getValue().getCount() / absEntries * 100);
		}

		drawChartNoLabels(absDCollector, path + "\\overview\\rel", "overview");
		drawChartLabeled(absDCollector, path + "\\overview\\rel", "overview");
	}

	public void drawChartNoLabels(final DataCollector dataCollector, final String path, final String title) {
		final var barChartHor = new DrawBarChartHorizontal(title);
		barChartHor.addSeries(new SeriesCollector(dataCollector));

		saveImage(barChartHor.getBarChartScene(IMAGE_WIDTH, IMAGE_HEIGHT).snapshot(null), path + "_hor.png");

		final var barChartVer = new DrawBarChartVertical(title);
		barChartVer.addSeries(new SeriesCollector(dataCollector));

		saveImage(barChartVer.getBarChartScene(IMAGE_WIDTH, IMAGE_HEIGHT).snapshot(null), path + "_ver.png");

	}

	public void drawChartLabeled(final DataCollector dataCollector, final String path, final String title) {
		dataCollector.addLabels();

		final var barChartHor = new DrawBarChartHorizontal(title);
		barChartHor.addSeries(new SeriesCollector(dataCollector));

		saveImage(barChartHor.getBarChartScene(IMAGE_WIDTH, IMAGE_HEIGHT).snapshot(null), path + "_hor_labeled.png");

		final var barChartVer = new DrawBarChartVertical(title);
		barChartVer.addSeries(new SeriesCollector(dataCollector));

		saveImage(barChartVer.getBarChartScene(IMAGE_WIDTH, IMAGE_HEIGHT).snapshot(null), path + "_ver_labeled.png");
	}
}
