# Setup

This is how you setup Eclipse so that everything compiles.


## Install

### JDK

Before we install Eclipse make sure that you have the latest JDK installed.
(Under Windows: Make sure that you have specified `JAVA_HOME`.)

We use JDK 10.


### Maven

Download and install Maven.
(Follow the instruction on the website.)


### Eclipse

Download Eclipse (Java package with with git and maven!) and extract it in any directory you like.

Upon first start it will ask you for a workspace.
Again, use any directory you like or just go with the default.
(I recommend to check the box for 'use this as default'.)

#### Configuring Eclipse

There are a few options you want to change in Eclipse to make it easier to use.
Use the search bar to search for a title of each of the following points.

1. Save Actions
	First check all boxes and then click _Configure..._ under _Additional cations_. Check the following boxes: _Remove trailing whitespace_, _Correct indentation_, _Convert 'for' loops to enhanced_, _Use parentheses in expressions_, _Use modifier 'final' where possible_ (check all), _Convert functional interfaces instances_, _Use 'this' qualifier for filed accesses_, _Use 'this' qualifier for method accesses_, _Remove unused imports_, and all boxes under _Unnecessary code_. Leave the rest of the options as is.


## Import Projects

Open Eclipse and go to _File > Import_.
Search for `maven`, select _Existing Maven projects_, and click _Next_.

Select the root directory of the git repository as _Root directory_.

Select all projects and click _Finish_.

It might be necessary to add the Java 10 JRE.
Go to _Help > Preferences_ and search for _Installed JREs_.
In case you don't see something like `jre-10.x.x`, click `Add...`, select _Standard VM_, click _Next_, click _Directory..._, select the directory of you JRE e.g. `C:\Program Files\Java\jre-10.0.1`, and click _Finish_.
Now check the box in front of the newly added JRE to use it as default.
Click _Apply_.

Now go to _Execution Environments_.
Select _Java Se-10_ and check the JRE you just added.
Click _Apply and close_ and wait for Eclipse to rebuild your project.

If it still does not compile right-click the project and open _Properties_.
Go to _Java Build Path > Libraries_, select _JRE System Library [...]_, and click _Edit..._.
Select _JavaSe-10_ as _Execution environment_.
Click _Finish_, click _Apply and close_.

## Build

To build the project, you first have to go to the `jwarcex/` directory and run the `mvn` command.